#!/bin/bash

# Lol why have standards that match between browsers - need to make some changes
cd web-ext-artifacts
unzip passit.zip
rm passit.zip

# Remove unused chrome permission
sed -i '/clipboardWrite/d' manifest.json

# Zip it back up
zip -r passit.zip .
cd ..

echo 'Get access token'
ACCESS_TOKEN=$(curl "https://accounts.google.com/o/oauth2/token" -d "client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&refresh_token=${REFRESH_TOKEN}&grant_type=refresh_token&redirect_uri=urn:ietf:wg:oauth:2.0:oob" | jq -r .access_token)

echo "Submit passit.zip to chromewebstore items with app id ${APP_ID}"
curl -H "Authorization: Bearer ${ACCESS_TOKEN}" -H "x-goog-api-version: 2" -X PUT -T web-ext-artifacts/passit.zip -v "https://www.googleapis.com/upload/chromewebstore/v1.1/items/${APP_ID}"

echo "Publish to store!"
curl -H "Authorization: Bearer ${ACCESS_TOKEN}" -H "x-goog-api-version: 2" -H "Content-Length: 0" -X POST -v "https://www.googleapis.com/chromewebstore/v1.1/items/${APP_ID}/publish"
