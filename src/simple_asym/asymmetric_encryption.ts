/* tslint:disable:max-classes-per-file */
import {
  decrypt,
  decryptPrivateKey,
  DefaultBinary,
  encrypt,
  encryptPrivateKey,
  fromBase64,
  fromUTF8,
  generateKeys,
  generateSymmetricKey,
  rsaDecrypt,
  rsaEncrypt,
  toBase64,
  toUTF8
} from "./crypto";

// the <type>Brand attributes are so that we can't mistake one type of binary data for another
// based on https://basarat.gitbooks.io/typescript/content/docs/tips/nominalTyping.html#using-interfaces

export { sodium } from "./crypto";

export interface IBinary {
  readonly binary: DefaultBinary;
  readonly string: string;
}

export abstract class Binary implements IBinary {
  protected abstract binaryToString: (binary: DefaultBinary) => string;
  protected abstract stringToBinary: (s: string) => DefaultBinary;
  private binaryVal: DefaultBinary;
  private stringVal: string;
  constructor(data: DefaultBinary | string) {
    if (typeof data === "string") {
      this.stringVal = data;
    } else {
      this.binaryVal = data;
    }
  }

  get string(): string {
    if (this.stringVal === undefined) {
      this.stringVal = this.binaryToString(this.binaryVal);
    }
    return this.stringVal;
  }

  get binary(): DefaultBinary {
    if (this.binaryVal === undefined) {
      this.binaryVal = this.stringToBinary(this.stringVal);
    }
    return this.binaryVal;
  }
}

export class Base64Binary extends Binary {
  protected binaryToString = toBase64;
  protected stringToBinary = fromBase64;
}

export class UTF8Binary extends Binary {
  protected binaryToString = toUTF8;
  protected stringToBinary = fromUTF8;
}

export type IBinaryConstructor<T> = new (data: DefaultBinary) => T;

export interface IKey<DataT extends IBinary> {
  decrypt(
    encrypted: Encrypted<IKey<DataT>, DataT>,
    cons: IBinaryConstructor<DataT>
  ): DataT;
}

export class Encrypted<
  KeyT extends IKey<DataT>,
  DataT extends IBinary
> extends Base64Binary {}

export class SymmetricKey extends Base64Binary {
  public static generate(): SymmetricKey {
    return new SymmetricKey(generateSymmetricKey());
  }

  public encrypt<T extends IBinary>(data: T): Encrypted<SymmetricKey, T> {
    return new Encrypted<SymmetricKey, T>(encrypt(this.binary, data.binary));
  }

  public decrypt<T extends IBinary>(
    encrypted: Encrypted<SymmetricKey, T>,
    cons: IBinaryConstructor<T>
  ): T {
    return new cons(decrypt(this.binary, encrypted.binary));
  }
}

export class PublicKey extends Base64Binary {
  public encrypt<T extends IBinary>(data: T): Encrypted<KeyPair, T> {
    return new Encrypted<KeyPair, T>(rsaEncrypt(this.binary, data.binary));
  }
}

export class PrivateKey extends Base64Binary {}

export class KeyPair {
  public static generate(): KeyPair {
    const { privateKey, publicKey, keyType } = generateKeys();
    return new KeyPair(
      new PublicKey(publicKey),
      new PrivateKey(privateKey),
      keyType
    );
  }
  constructor(
    public readonly publicKey: PublicKey,
    public readonly privateKey: PrivateKey,
    readonly keyType = "curve25519"
  ) {}

  public decrypt<T extends IBinary>(
    encrypted: Encrypted<KeyPair, T>,
    cons: IBinaryConstructor<T>
  ): T {
    return new cons(
      rsaDecrypt(
        this.publicKey.binary,
        this.privateKey.binary,
        encrypted.binary
      )
    );
  }
}

export class Password extends UTF8Binary {
  public encrypt(privateKey: PrivateKey): Encrypted<Password, PrivateKey> {
    return new Encrypted<Password, PrivateKey>(
      encryptPrivateKey(privateKey.binary, this.string)
    );
  }
  public decrypt(encrypted: Encrypted<Password, PrivateKey>): PrivateKey {
    return new PrivateKey(decryptPrivateKey(encrypted.binary, this.string));
  }
}
