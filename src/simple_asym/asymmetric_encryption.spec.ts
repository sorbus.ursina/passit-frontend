import {
  Base64Binary,
  Encrypted,
  KeyPair,
  Password,
  PrivateKey,
  PublicKey,
  sodium,
  SymmetricKey,
  UTF8Binary
} from "./asymmetric_encryption";

describe("Encryption can", () => {
  beforeEach(async () => {
    await sodium.ready;
  });

  it("can create UT8 encoded binary", () => {
    const ut8String = "Hey teher! ☢";
    const b = new UTF8Binary(ut8String);
    expect(b.string).toBe(ut8String);
    expect(b.binary).toBeTruthy();
  });

  it("can create base 64 encoded binary", () => {
    const base64String = "TCkVmbGlDxi9t+ZIYeYTgRLoHCe7kmW4AhfS8/VKqkI=";
    const b = new Base64Binary(base64String);
    expect(b.string).toBe(base64String);
    expect(b.binary).toBeTruthy();
  });

  it("make RSA keys and ensure they are a string that can be saved", () => {
    const keys = KeyPair.generate();
    expect(keys.publicKey.string.length).toBe(44);
    expect(keys.privateKey.string.length).toBe(44);
    expect(typeof keys.privateKey.string).toBe("string");
  });

  it("encrypt RSA key with password", () => {
    const keys = KeyPair.generate();
    const password = new Password("123456");
    const encryptedKey = password.encrypt(keys.privateKey);
    expect(encryptedKey.string.length).toBe(120);
    expect(typeof encryptedKey.string).toBe("string");
  });

  it("import RSA key pair", () => {
    const privateKeyBase64 = "TCkVmbGlDxi9t+ZIYeYTgRLoHCe7kmW4AhfS8/VKqkI=";
    const publicKeyBase64 = "wL+JHgcJs5aRYMjF8QmHcUVCWdE5ENzLVsKEf9V2UHM=";
    const keyPair = new KeyPair(
      new PublicKey(publicKeyBase64),
      new PrivateKey(privateKeyBase64)
    );
    expect(keyPair).toBeTruthy();
  });

  it("import RSA key pair with encrypted private key", () => {
    // tslint:disable-next-line:max-line-length
    const privateKeyBase64 =
      "HNP8i6zQS6Kj1b0tovnnySFCfLHWJ7ZBKfHCqperfAbgRXdlabRgRtTbb2ZyT25isGkg390tKGn+rE7emNgbkqvf9Qd6CgG37pclw0UUQy9TruasDA3/lQ==";
    const privateKeyEncr = new Encrypted<Password, PrivateKey>(
      privateKeyBase64
    );
    const password = new Password("123456");

    // Try with bad password
    expect(() => new Password("nope").decrypt(privateKeyEncr)).toThrow();

    const privateKey = password.decrypt(privateKeyEncr);
    expect(privateKey).toBeTruthy();
  });

  it("encrypt and decrypt using RSA public key", () => {
    const plaintext = "hi☢⛴";

    const privateKeyBase64 = "TCkVmbGlDxi9t+ZIYeYTgRLoHCe7kmW4AhfS8/VKqkI=";
    const publicKeyBase64 = "wL+JHgcJs5aRYMjF8QmHcUVCWdE5ENzLVsKEf9V2UHM=";
    const keyPair = new KeyPair(
      new PublicKey(publicKeyBase64),
      new PrivateKey(privateKeyBase64)
    );

    const ciphertext = keyPair.publicKey.encrypt(new UTF8Binary(plaintext));
    expect(ciphertext.string).not.toEqual(plaintext);
    const decrypted = keyPair.decrypt(ciphertext, UTF8Binary);
    expect(decrypted.string).toBe(plaintext);
  });

  it("generate symmetric key", () => {
    const key = SymmetricKey.generate();
    expect(key).toBeDefined();
    expect(typeof key.string).toBe("string");
  });

  it("set symmetric key", () => {
    const privateKeyBase64 = "TCkVmbGlDxi9t+ZIYeYTgRLoHCe7kmW4AhfS8/VKqkI=";
    const publicKeyBase64 = "wL+JHgcJs5aRYMjF8QmHcUVCWdE5ENzLVsKEf9V2UHM=";
    const keyPair = new KeyPair(
      new PublicKey(publicKeyBase64),
      new PrivateKey(privateKeyBase64)
    );
    const key = SymmetricKey.generate();
    const encryptedKey = keyPair.publicKey.encrypt(key);
    expect(encryptedKey.string).not.toEqual(key.string);
    const unencryptedKey = keyPair.decrypt(encryptedKey, SymmetricKey);
    expect(unencryptedKey.string).toEqual(key.string);
  });

  it("encrypt and decrypt using symmetric key", () => {
    const key = new SymmetricKey(
      "HMS11YymAKX5z6d6/hhdvyGtj7wiTfwzaO0O3ptSHZ4="
    );
    const message = new UTF8Binary("hello");
    const ciphertext = key.encrypt(message);
    const decrypted = key.decrypt(ciphertext, UTF8Binary);
    expect(decrypted.binary).toEqual(message.binary);
  });

  it("bob and alice integration test", () => {
    // Generate key pairs for Bob and for Alice
    const alice = KeyPair.generate();

    // Generate one secret key for symmetric encryption
    const bobSymmetricKey = SymmetricKey.generate();

    // Send key to alice ༼つ◕_◕ ༽つ
    const encryptedSymmetricKey = alice.publicKey.encrypt(bobSymmetricKey);
    // Maybe encryptedSymmetricKey is saved in a database owned by Evil Server.
    // Evil Server is watching encryptedSymmetricKey
    // But can't figure out what it is
    // Alice can decrypt the secret key now, with her private key
    const aliceSymmetricKey = alice.decrypt(
      encryptedSymmetricKey,
      SymmetricKey
    );

    // Now that alice has the secret key, we can send a long message
    const message = new UTF8Binary("Do you know what cryptobox means?");
    const ciphertext = bobSymmetricKey.encrypt(message);
    expect(ciphertext.binary).not.toEqual(message.binary);
    // Evil server is still watching....maybe as bob saves it to that database again

    // Alice can decrypt the message. Perhaps this is happening locally and not
    // where Evil Server can watch
    const decryptedMessage = aliceSymmetricKey.decrypt(ciphertext, UTF8Binary);

    // Alice can send back data easy now without RSA encryption.
    // She could also choose to generate a new RSA key instead and start over.
    aliceSymmetricKey.encrypt(
      new UTF8Binary("Yes but why is crypto so hard to use?")
    );
    // Send a message
    // It should be base64 (not binary)
    expect(typeof ciphertext.string).toEqual("string");
    expect(decryptedMessage.string).toEqual(message.string);
  });
});
