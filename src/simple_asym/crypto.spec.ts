import * as sodium from "libsodium-wrappers";
import { HASH_MEMORY } from "./constants";
import { Buffer } from "buffer";
import {
  authenticatedEncryption,
  decrypt,
  decryptPrivateKey,
  encrypt,
  encryptPrivateKey,
  fromBase64,
  fromUTF8,
  generateKeys,
  hashPassword,
  makeRandomTypableString,
  rsaDecrypt,
  rsaEncrypt,
  toBase64,
  toUTF8,
  verifyOwner,
} from "./crypto";

describe("crypto can", () => {
  beforeEach(async () => {
    await sodium.ready;
  });

  // Requires in browser usage, so disabled in CI
  xit("Uses wasm", async () => {
    expect((sodium as any).libsodium.usingWasm).toBe(true);
  });

  it("makes random string", () => {
    const generated = makeRandomTypableString();
    expect(generated).toBeTruthy();
    expect(generated.indexOf("undefined")).toBe(-1);
  });

  // These boring tests without expects are  nice because they fail sometimes when libsodium.js breaks.
  it("test crypto_box_keypair", async () => {
    sodium.crypto_box_keypair();
  });

  it("test hash", async () => {
    const password = "hunter2";
    const salt = new Uint8Array([
      88,
      240,
      185,
      66,
      195,
      101,
      160,
      138,
      137,
      78,
      1,
      2,
      3,
      4,
      5,
      6,
    ]);
    sodium.crypto_pwhash(
      sodium.crypto_box_SEEDBYTES,
      password,
      salt,
      sodium.crypto_pwhash_OPSLIMIT_INTERACTIVE,
      HASH_MEMORY / 2,
      sodium.crypto_pwhash_ALG_DEFAULT
    );
  });

  it("test hash speed", async () => {
    const password = "hunter2";
    const salt = new Uint8Array([
      88,
      240,
      185,
      66,
      195,
      101,
      160,
      138,
      137,
      78,
      1,
      2,
      3,
      4,
      5,
      6,
    ]);
    const t0 = performance.now();
    await hashPassword(password, salt, 2);
    const t1 = performance.now();
    console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.");
  });

  it("test base64", () => {
    const testB64 = "2A28PjsDW/WpQqMXPLTIkL99VwnofGRqNL7wdJrzUyo=";
    sodium.from_base64(testB64, 1);
  });

  it("make and export RSA key", () => {
    const keys = generateKeys();
    expect(toBase64(keys.privateKey).length).toBe(44);
    expect(toBase64(keys.publicKey).length).toBe(44);
  });

  it("export and import private key with password", () => {
    const privateB64 = "2A28PjsDW/WpQqMXPLTIkL99VwnofGRqNL7wdJrzUyo=";
    const password = "hunter2";
    const encr = encryptPrivateKey(fromBase64(privateB64), password);
    const decr = decryptPrivateKey(encr, password);
    expect(privateB64).toEqual(toBase64(decr));
  });

  it("hash a password", () => {
    const password = "hunter2";
    const salt = new Uint8Array([
      88,
      240,
      185,
      66,
      195,
      101,
      160,
      138,
      137,
      78,
      1,
      2,
      3,
      4,
      5,
      6,
    ]);
    const hash = hashPassword(password, salt);
    expect(hash[0]).toEqual(49);
  });

  it("encrypt private key with password", () => {
    const password = "hunter2";
    const keys = generateKeys();
    const encr = encryptPrivateKey(keys.privateKey, password);
    expect(encr).toBeTruthy();
  });

  it("RSA Encrypt and Decrypt", () => {
    const keys = generateKeys();
    const message = "test Ơ";
    const ciphertext = rsaEncrypt(keys.publicKey, fromUTF8(message));
    expect(ciphertext).toBeTruthy();
    expect(toBase64(ciphertext)).not.toEqual(message);
    const decryptedMessage = rsaDecrypt(
      keys.publicKey,
      keys.privateKey,
      ciphertext
    );
    expect(toUTF8(decryptedMessage)).toEqual(message);
  });

  it("Prove identity", () => {
    const keysBob = generateKeys();
    const keysAlice = generateKeys();
    const message = sodium.from_string("bleh");
    const ciphertext = authenticatedEncryption(
      keysBob.publicKey,
      keysAlice.privateKey,
      message
    );

    // Incorrect usage, expect failure.
    let verified = verifyOwner(
      keysBob.publicKey,
      keysBob.privateKey,
      ciphertext
    );
    expect(verified).toBeFalsy();

    verified = verifyOwner(keysAlice.publicKey, keysBob.privateKey, ciphertext);
    expect(verified).toBeTruthy();
  });

  it("Encrypt and decrypt text", () => {
    const key = Buffer.from(
      "724b092810ec86d7e35c9d067702b31ef90bc43a7b598626749914d6a3e033ed",
      "hex"
    );
    const message = "hell dfd";
    const ciphertext = encrypt(key, fromUTF8(message));
    expect(toBase64(ciphertext)).not.toEqual(message);
    const decryptedMessage = decrypt(key, ciphertext);
    expect(toUTF8(decryptedMessage)).toBe(message);
  });
});
