export const environment = {
  production: true,
  extension: false,
  docker: false,
  VERSION: require("../../package.json").version
};
