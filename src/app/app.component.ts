import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { Router, RoutesRecognized } from "@angular/router";
import { Store, select } from "@ngrx/store";
import { MicroSentryService } from "@micro-sentry/angular";
import { filter, take } from "rxjs/operators";

import { environment } from "../environments/environment";
import * as fromRoot from "./app.reducers";
import { GetConfAction } from "./get-conf/conf.actions";
import { AppDataService } from "./shared/app-data/app-data.service";
import { combineLatest } from "rxjs";

export const DSN_REGEXP =
  /^(?:(\w+):)\/\/(?:(\w+)(?::(\w+))?@)([\w.-]+)(?::(\d+))?\/(.+)/;

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: "app-root",
  template: `
    <navbar-container></navbar-container>
    <main><router-outlet></router-outlet></main>
  `
})
export class AppComponent implements OnInit {
  public title: string;

  constructor(
    private appDataService: AppDataService,
    public store: Store<fromRoot.IState>,
    private router: Router,
    private titleService: Title,
    private microSentry: MicroSentryService
  ) {
    this.appDataService.rehydrate();

    this.title = "Passit";

    this.router.events.subscribe(event => {
      if (event instanceof RoutesRecognized) {
        let titleTag = "Passit";
        if (typeof event.state.root.firstChild!.data["title"] !== "undefined") {
          titleTag = event.state.root.firstChild!.data["title"] + " | Passit";
        }
        this.titleService.setTitle(titleTag);
      }
    });
  }

  ngOnInit() {
    this.store.dispatch(new GetConfAction());
    combineLatest([
      this.store.pipe(select(fromRoot.getConfState)),
      this.store.pipe(select(fromRoot.getOptInErrorReporting)),
    ]).pipe(
        filter(([conf, optIn]) => conf.ravenDsn !== null && optIn),
        take(1),
      )
      .subscribe(([conf, optIn]) => {
        if (conf.ravenDsn) {
          const searched = DSN_REGEXP.exec(conf.ravenDsn);
          const dsn = searched ? searched.slice(1) : [];
          const pathWithProjectId = dsn[5].split("/");
          const path = pathWithProjectId.slice(0, -1).join("/");

          (this.microSentry.apiUrl as string) =
            dsn[0] +
            "://" +
            dsn[3] +
            (dsn[4] ? ":" + dsn[4] : "") +
            (path ? "/" + path : "") +
            "/api/" +
            pathWithProjectId.pop() +
            "/store/";

          (this.microSentry.authHeader as string) =
            "Sentry sentry_version=7,sentry_key=" +
            dsn[1] +
            (dsn[2] ? ",sentry_secret=" + dsn[2] : "");

          if (conf.environment) {
            (this.microSentry.environment as string) = conf.environment;
          }
          (this.microSentry as any).release = environment.VERSION;
        }
      });
  }
}
