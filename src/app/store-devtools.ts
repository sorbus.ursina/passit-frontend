// This is set in this file so that it can be removed when building for production
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

export const devtoolsModule = StoreDevtoolsModule.instrument({maxAge: 25});

