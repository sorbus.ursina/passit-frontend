// tslint:disable:directive-selector
// tslint:disable:directive-class-suffix
import {
  Directive,
  Input,
  forwardRef,
  HostListener,
  Renderer2,
  ElementRef
} from "@angular/core";

import {
  FormViewAdapter,
  NGRX_FORM_VIEW_ADAPTER,
  FormControlState
} from "ngrx-forms";

@Directive({
  selector: "TextField[ngrxFormControlState]",
  providers: [
    {
      provide: NGRX_FORM_VIEW_ADAPTER,
      useExisting: forwardRef(() => NgrxTextFieldViewAdapter),
      multi: true
    }
  ]
})
export class NgrxTextFieldViewAdapter implements FormViewAdapter {
  onChange: (value: any) => void = () => void 0;

  @HostListener("blur")
  onTouched: () => void = () => void 0;

  constructor(private renderer: Renderer2, private elementRef: ElementRef) {}

  @Input()
  set ngrxFormControlState(value: FormControlState<any>) {
    if (!value) {
      throw new Error("The control state must not be undefined!");
    }
  }

  setViewValue(value: any): void {
    const normalizedValue = value == null ? "" : value;
    this.renderer.setProperty(
      this.elementRef.nativeElement,
      "text",
      normalizedValue
    );
  }

  setOnChangeCallback(fn: (value: any) => void): void {
    this.onChange = fn;
  }

  setOnTouchedCallback(fn: () => void): void {
    this.onTouched = fn;
  }

  setIsDisabled(isDisabled: boolean): void {
    this.renderer.setProperty(
      this.elementRef.nativeElement,
      "isEnabled",
      !isDisabled
    );
  }

  @HostListener("textChange", ["$event"])
  handleInput(event: any): void {
    this.onChange(event.object.text);
  }
}

@Directive({
  selector: "TextView[ngrxFormControlState]",
  providers: [
    {
      provide: NGRX_FORM_VIEW_ADAPTER,
      useExisting: forwardRef(() => NgrxTextViewViewAdapter),
      multi: true
    }
  ]
})
export class NgrxTextViewViewAdapter extends NgrxTextFieldViewAdapter {}
