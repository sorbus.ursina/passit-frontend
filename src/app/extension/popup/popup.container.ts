import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Store, select } from "@ngrx/store";

import * as fromRoot from "../../app.reducers";
import {
  getPopupSecrets,
  getPopupSearch,
  getPopupSelected,
  getMatchedSecrets,
  getPopupUsernameCopied,
  getPopupPasswordCopied,
  getPopupFirstTimeLoadingComplete
} from "./popup.selectors";
import * as secretActions from "../../secrets/secret.actions";
import { SecretService } from "../../secrets/secret.service";
import {
  SetCurrentUrlAction,
  SetSearchAction,
  SetSelectedSecretAction,
  NullSelectedSecretAction,
  CopyUsername,
  CopyPassword
} from "./popup.actions";
import { HotkeysService, Hotkey } from "angular2-hotkeys";
import { ISecret } from "../../../passit_sdk/api.interfaces";
import { IFormFillRequest } from "../interfaces";
import { loadGroups } from "../../data/groups.actions";
import { selectHasGroupInvites } from "../../data/group.selectors";
const sanitizeUrl = require("@braintree/sanitize-url").sanitizeUrl;

@Component({
  selector: "app-popup-container",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <app-popup
      [secrets]="secrets$ | async"
      [selectedSecret]="selectedSecret$ | async"
      [search]="search$ | async"
      [formFillMessage]="formFillMessage"
      [matchedSecrets]="matchedSecrets$ | async"
      [usernameCopied]="usernameCopied$ | async"
      [passwordCopied]="passwordCopied$ | async"
      [totalSecretsCount]="totalSecretsCount$ | async"
      [popupFirstTimeLoadingComplete]="popupFirstTimeLoadingComplete$ | async"
      [hasGroupInvites]="hasGroupInvites$ | async"
      (setSelected)="setSelected($event)"
      (closeSelected)="closeSelected()"
      (searchUpdate)="searchUpdate($event)"
      (openUrl)="openUrl($event)"
      (signIn)="signIn($event)"
      (onCopyUsername)="copyUsername($event)"
      (onCopyPassword)="copyPassword($event)"
      (openFullApp)="openFullApp($event)"
    ></app-popup>
  `
})
export class PopupContainer implements OnInit {
  secrets$ = this.store.pipe(select(getPopupSecrets));
  search$ = this.store.pipe(select(getPopupSearch));
  totalSecretsCount$ = this.store.pipe(select(fromRoot.getSecretsCount));
  selectedSecret$ = this.store.pipe(select(getPopupSelected));
  matchedSecrets$ = this.store.pipe(select(getMatchedSecrets));
  usernameCopied$ = this.store.pipe(select(getPopupUsernameCopied));
  passwordCopied$ = this.store.pipe(select(getPopupPasswordCopied));
  popupFirstTimeLoadingComplete$ = this.store.pipe(
    select(getPopupFirstTimeLoadingComplete)
  );
  hasGroupInvites$ = this.store.pipe(select(selectHasGroupInvites));
  formFillMessage = "";

  constructor(
    private store: Store<fromRoot.IState>,
    private secretService: SecretService,
    private _hotkeysService: HotkeysService
  ) {}

  ngOnInit() {
    // TODO make this not happen EVERY time
    this.setupHotkey();
    this.getSecrets();
    this.getGroups();
    this.checkUrlForMatch();
  }

  private setupHotkey() {
    this._hotkeysService.add(
      new Hotkey(
        "ctrl+shift+l",
        (event: KeyboardEvent): boolean => {
          this.matchedSecrets$.subscribe(secrets =>
            secrets.length > 0 ? this.signIn(secrets[0]) : null
          );
          return false;
        },
        ["input"]
      )
    );
  }

  getSecrets() {
    this.store.dispatch(new secretActions.GetSecretsAction());
  }

  getGroups() {
    this.store.dispatch(loadGroups());
  }

  setSelected(selected: number) {
    this.store.dispatch(new SetSelectedSecretAction(selected));
    this.formFillMessage = "";
  }

  closeSelected() {
    this.store.dispatch(new NullSelectedSecretAction());
  }

  searchUpdate(term: string) {
    this.store.dispatch(new SetSearchAction(term));
  }

  openFullApp(route: string) {
    browser.tabs.create({
      url: "/index.html" + route
    });
    window.close();
  }

  copyUsername(secret: ISecret) {
    this.store.dispatch(new CopyUsername(secret));
  }

  copyPassword(secret: ISecret) {
    this.store.dispatch(new CopyPassword(secret));
  }

  signIn(secret: ISecret) {
    const username = secret.data["username"]!;
    this.secretService.showSecret(secret).then(decrypted => {
      const password = decrypted["password"];
      this.executeAutofillScript(username, password).then(() => {
        window.close();
      });
    });
  }

  checkUrlForMatch() {
    // TODO critical path for potential security audit
    // This url comes from the current tab domain - it could attempt to inject code.
    this.getActiveTab().then(tab => {
      if (tab && tab.url !== undefined) {
        const url: string = sanitizeUrl(tab.url);
        this.store.dispatch(new SetCurrentUrlAction(url));
      }
    });
  }

  /** Run the autofill script on the current tab, requires using the activeTab permission.
   */
  executeAutofillScript(username: string, password: string) {
    return this.getActiveTab().then(tab => {
      if (!tab) {
        return;
      }

      // critical path for potential security audit
      const loginRequest: IFormFillRequest = {
        origin: new URL(tab.url!).origin,
        fields: ["login", "secret"],
        login: {
          fields: {
            login: username.substring(0, 200),
            secret: password.substring(0, 200)
          }
        }
      };
      return browser.tabs
        .executeScript(tab.id, {
          file: "/inject.js",
          allFrames: true
        })
        .then(() => {
          browser.tabs.executeScript(tab.id!, {
            code: `window.browserpass.fillLogin(${JSON.stringify(
              loginRequest
            )})`,
            allFrames: true
          });
        });
    });
  }

  /** Firefox and Chrome compatible way to get the active tab. tabs.getCurrent does not work in Firefox.
   * from https://stackoverflow.com/a/41943267
   */
  async getActiveTab() {
    if (browser.tabs) {
      const tabs = await browser.tabs.query({
        active: true,
        currentWindow: true
      });
      return tabs[0];
    } else {
      console.warn("no browser.tabs in popup, are you debugging?");
    }
  }

  openUrl(secret: ISecret) {
    let url = secret.data["url"];
    if (url !== undefined) {
      // Add http protocol if not specified (ftp, etc not supported)
      if (url.startsWith("http") === false) {
        url = "http://" + url;
      }
      url = sanitizeUrl(url);
      return browser.tabs.create({ url });
    }
  }
}
