import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ISecret } from "../../../../passit_sdk/api.interfaces";

@Component({
  selector: "popup-onboarding",
  templateUrl: "./popup-onboarding.component.html",
  styleUrls: ["./popup-onboarding.component.scss"]
})
export class PopupOnboardingComponent {
  @Input() totalSecretsCount: number;
  @Input() secret: ISecret;

  @Output() openFullApp = new EventEmitter();
  @Output() openUrl = new EventEmitter();
}
