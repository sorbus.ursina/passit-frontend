import {
  Component,
  EventEmitter,
  Input,
  Output,
  ChangeDetectionStrategy
} from "@angular/core";
import { AbstractControlState } from "ngrx-forms";
import { IMultiselectList } from "./multiselect-list/multiselect-list.component";

@Component({
  selector: "app-multiselect",
  templateUrl: "./multiselect.component.html",
  styleUrls: ["./multiselect.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiselectComponent {
  @Input() label: string;
  @Input() userId: number;
  @Input() searchControl: AbstractControlState<string>;
  _listItems: IMultiselectList[];
  selectedItems: IMultiselectList[];
  @Input("listItems") set listItems(listItems: IMultiselectList[]) {
    // Don't show yourself in new group
    this._listItems = listItems.filter(
      listItem => listItem.value !== this.userId
    );
    this.selectedItems = listItems.filter(item => item.isSelected);
  }
  get listItems() {
    return this._listItems;
  }
  /**
   * For the group member case; if you're in a group by yourself we don't want
   * the trash icon to be visible, we just want the user to press the delete
   * button at that point.
   */
  @Input() singleItemIsRemovable = true;
  /** Show this message before allowing removal, based on listItem.confirmRemoval */
  @Input() confirmRemovalMessage: string;
  @Input() keyControls: boolean;
  @Input() searchFieldValidationPattern: RegExp;
  @Input() invalidSearchFieldText: string;
  @Input() noSearchFieldMatchText: string;
  @Input() noListItemsText: string;
  @Input() fewListItemsText: string;
  @Input() allListItemsSelectedText: string;
  @Input() justAddedText: string;
  @Output() toggleKeyControls = new EventEmitter();
  @Output() removeListItem: EventEmitter<number> = new EventEmitter();
  @Output() addListItem: EventEmitter<number> = new EventEmitter();

  /**
   * The multiselect add component displays differently based on this. It needs
   * to be in this component because its template has a button to trigger in it
   */
  modalMode = false;

  toggleModalMode() {
    this.modalMode = !this.modalMode;
  }
}
