import { createSelector } from "@ngrx/store";
import { getUserId, selectAllGroups } from "../app.reducers";
import { ISelectOptions } from "./interfaces";

/** Get pending group invites */
export const selectPendingGroups = createSelector(
  selectAllGroups,
  getUserId,
  (groups, userId) =>
    groups.filter(group =>
      group.groupuser_set.find(
        groupuser => groupuser.user === userId && groupuser.is_invite_pending
      )
    )
);

export const selectPendingGroupsCount = createSelector(
  selectPendingGroups,
  groups => groups.length
);

/** Get active groups that have accepted invite status */
export const selectActiveGroups = createSelector(
  selectAllGroups,
  getUserId,
  (groups, userId) =>
    groups.filter(
      group =>
        !group.groupuser_set.find(
          groupuser => groupuser.user === userId && groupuser.is_invite_pending
        )
    )
);

export const selectActiveGroupsCount = createSelector(
  selectActiveGroups,
  groups => groups.length
);

/** Determines if any group invites exist */
export const selectHasGroupInvites = createSelector(
  selectAllGroups,
  getUserId,
  (groups, userId) => {
    return (
      typeof groups.find(group => {
        return (
          typeof group.groupuser_set.find(
            groupuser =>
              groupuser.user === userId && groupuser.is_invite_pending
          ) !== "undefined"
        );
      }) !== "undefined"
    );
  }
);

/** Put groups in a format that select2 can use. */
export const selectActiveGroupsAsOptions = createSelector(
  selectActiveGroups,
  groups => {
    const activeGroupsAsOptions: ISelectOptions[] = [];
    groups.forEach(group =>
      activeGroupsAsOptions.push({
        value: group.id,
        label: group.name
      })
    );
    activeGroupsAsOptions.push({
      value: 0,
      label: "No Group"
    });
    return activeGroupsAsOptions;
  }
);
