import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { API_URL } from "../constants";
import { NewOrganization, Organization } from "./organizations.interfaces";

@Injectable({
  providedIn: "root",
})
export class OrganizationsService {
  private readonly baseURL = API_URL + "organizations/";
  constructor(private http: HttpClient) {}

  createOrganization(organization: NewOrganization) {
    return this.http.post<Organization>(this.baseURL, organization);
  }

  listOrganizations() {
    return this.http.get<Organization[]>(this.baseURL);
  }

  updateOrganization(id: number, organization: Organization) {
    return this.http.put(this.detailURL(id), organization);
  }

  deleteOrganization(id: number) {
    return this.http.delete(this.detailURL(id));
  }

  detailURL(id: number) {
    return `${this.baseURL}${id}/`;
  }
}
