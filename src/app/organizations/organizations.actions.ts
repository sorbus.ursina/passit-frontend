import { createAction, props } from "@ngrx/store";
import { Organization } from "./organizations.interfaces";

export const getOrganizationList = createAction(
  "Organizations] Get Organizations"
);

export const retrievedOrganizationList = createAction(
  "[Organization List/API] Retrieve Organizations Success",
  props<{ organizations: Organization[] }>()
);

export const showCreateOrganization = createAction(
  "[Organization] Show Create Organization"
);

export const hideCreateOrganization = createAction(
  "[Organization] Hide Create Organization"
);

export const createOrganization = createAction(
  "[Organization Create/API] Create Organization"
);

export const createOrganizationSuccess = createAction(
  "[Organization Create/API] Create Organization Success",
  props<{ organization: Organization }>()
);

export const setActiveOrganization = createAction(
  "[Organization] Set Active Organization",
  props<{ organizationID: number | null }>()
);
