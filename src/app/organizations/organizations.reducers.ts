import { createFeatureSelector, createReducer, on } from "@ngrx/store";
import {
  createFormGroupState,
  FormGroupState,
  onNgrxForms,
  setValue,
} from "ngrx-forms";
import { NewOrganization, Organization } from "./organizations.interfaces";
import * as fromRoot from "../app.reducers";
import {
  createOrganizationSuccess,
  retrievedOrganizationList,
  setActiveOrganization,
  showCreateOrganization,
  hideCreateOrganization,
} from "./organizations.actions";

// Proof of concept
// tslint:disable:no-empty-interface
export interface NewOrganizationFormValue extends NewOrganization {}

const NEW_FORM_ID = "New Organization";

const initialNewFormState = createFormGroupState<NewOrganizationFormValue>(
  NEW_FORM_ID,
  { name: "" }
);

// const validateAndUpdateNewFormState = updateGroup<NewOrganizationFormValue>({})

// const newFormReducer = createFormStateReducerWithUpdate<NewOrganizationFormValue>(
//   validateAndUpdateNewFormState
// );

export interface EditOrganizationFormValue extends NewOrganization {}

const EDIT_FORM_ID = "Edit Organization";

const initialEditFormState = createFormGroupState<EditOrganizationFormValue>(
  EDIT_FORM_ID,
  { name: "" }
);

// const validateAndUpdateEditFormState = updateGroup<EditOrganizationFormValue>(
//   {}
// );

// const editFormReducer = createFormStateReducerWithUpdate<EditOrganizationFormValue>(
//   validateAndUpdateEditFormState
// );

interface OrganizationsState {
  newOrganizationForm?: FormGroupState<NewOrganizationFormValue>;
  editOrganizationForm?: FormGroupState<EditOrganizationFormValue>;
  organizations: Organization[];
  activeOrganizationID: number | null;
  firstTimeLoadingComplete: boolean;
  showCreate: boolean;
}

export const initialState: OrganizationsState = {
  newOrganizationForm: initialNewFormState,
  editOrganizationForm: initialEditFormState,
  organizations: [],
  activeOrganizationID: null,
  firstTimeLoadingComplete: false,
  showCreate: false,
};

export const reducer = createReducer(
  initialState,
  onNgrxForms(),
  on(retrievedOrganizationList, (state, { organizations }) => ({
    ...state,
    organizations,
    activeOrganizationID:
      organizations.length && !state.firstTimeLoadingComplete
        ? organizations[0].id
        : state.activeOrganizationID,
    firstTimeLoadingComplete: true,
  })),
  on(showCreateOrganization, (state) => ({ ...state, showCreate: true })),
  on(hideCreateOrganization, (state) => ({
    ...state,
    showCreate: false,
    newOrganizationForm: initialState.newOrganizationForm,
  })),
  on(createOrganizationSuccess, (state, { organization }) => ({
    ...state,
    showCreate: false,
    activeOrganizationID: organization.id,
    editOrganizationForm: setValue(state.editOrganizationForm!, {
      name: organization.name,
    }),
    newOrganizationForm: initialState.newOrganizationForm,
  })),
  on(setActiveOrganization, (state, { organizationID }) => ({
    ...state,
    editOrganizationForm: setValue(state.editOrganizationForm!, {
      name:
        state.organizations.find((org) => org.id === organizationID)?.name ||
        "",
    }),
    activeOrganizationID: organizationID,
    showCreate: false,
  }))
);

export interface State extends fromRoot.IState {
  organizations: State;
}

export const selectOrganizationsState = createFeatureSelector<
  State,
  OrganizationsState
>("organizations");
