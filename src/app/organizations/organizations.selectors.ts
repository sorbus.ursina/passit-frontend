import { createSelector } from "@ngrx/store";
import { selectOrganizationsState } from "./organizations.reducers";

export const selectNewOrganizationForm = createSelector(
  selectOrganizationsState,
  (state) => state.newOrganizationForm
);

export const selectEditOrganizationForm = createSelector(
  selectOrganizationsState,
  (state) => state.editOrganizationForm
);

export const selectOrganizations = createSelector(
  selectOrganizationsState,
  (state) => state.organizations
);

export const selectOrganizationOptions = createSelector(
  selectOrganizations,
  (orgs) => orgs.map((org) => ({ value: org.id.toString(), label: org.name }))
);

export const selectOrganizationLoadingComplete = createSelector(
  selectOrganizationsState,
  (state) => state.firstTimeLoadingComplete
);

export const selectOrganizationShowCreate = createSelector(
  selectOrganizationsState,
  (state) => state.showCreate
);

export const selectOrganizationShowOnboarding = createSelector(
  selectOrganizationsState,
  (state) =>
    state.firstTimeLoadingComplete &&
    !state.showCreate &&
    state.organizations.length === 0
);

export const selectActiveOrganizationID = createSelector(
  selectOrganizationsState,
  (state) => state.activeOrganizationID
);

export const selectActiveOrganization = createSelector(
  selectActiveOrganizationID,
  selectOrganizations,
  (id, organizations) => organizations.find((org) => org.id === id)
);
