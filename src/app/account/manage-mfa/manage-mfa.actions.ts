import { Action } from "@ngrx/store";
import { IGeneratedMFA } from "./manage-mfa.interfaces";

export enum ManageMfaActionTypes {
  FORWARD_STEP = "[ENABLE MFA] Forward Step",
  ENABLE_MFA = "[ENABLE MFA] Enable",
  ENABLE_MFA_SUCCESS = "[ENABLE MFA] Enable Success",
  ENABLE_MFA_FAILURE = "[ENABLE MFA] Enable Failure",
  ACTIVATE_MFA = "[ENABLE MFA] Activate",
  ACTIVATE_MFA_SUCCESS = "[ENABLE MFA] Activate Success",
  ACTIVATE_MFA_FAILURE = "[ENABLE MFA] Activate Failure",
  DEACTIVATE_MFA = "[ENABLE MFA] Deactivate",
  DEACTIVATE_MFA_SUCCESS = "[ENABLE MFA] Deactivate Success",
  DEACTIVATE_MFA_FAILURE = "[ENABLE MFA] Deactivate Failure",
  RESET_FORM = "[Reset] = Reset Form"
}

export class ForwardStep implements Action {
  readonly type = ManageMfaActionTypes.FORWARD_STEP;
}

export class EnableMfa implements Action {
  readonly type = ManageMfaActionTypes.ENABLE_MFA;
}

export class EnableMfaSuccess implements Action {
  readonly type = ManageMfaActionTypes.ENABLE_MFA_SUCCESS;

  constructor(public payload: IGeneratedMFA) {}
}

export class EnableMfaFailure implements Action {
  readonly type = ManageMfaActionTypes.ENABLE_MFA_FAILURE;

  constructor(public payload: string[]) {}
}

export class ActivateMfa implements Action {
  readonly type = ManageMfaActionTypes.ACTIVATE_MFA;
}

export class ActivateMfaSuccess implements Action {
  readonly type = ManageMfaActionTypes.ACTIVATE_MFA_SUCCESS;
}

export class ActivateMfaFailure implements Action {
  readonly type = ManageMfaActionTypes.ACTIVATE_MFA_FAILURE;
}

export class DeactivateMfa implements Action {
  readonly type = ManageMfaActionTypes.DEACTIVATE_MFA;
}

export class DeactivateMfaSuccess implements Action {
  readonly type = ManageMfaActionTypes.DEACTIVATE_MFA_SUCCESS;
}

export class DeactivateMfaFailure implements Action {
  readonly type = ManageMfaActionTypes.DEACTIVATE_MFA_FAILURE;
}

export class ResetForm implements Action {
  readonly type = ManageMfaActionTypes.RESET_FORM;
}

export type ManageMfaActions =
  | ForwardStep
  | EnableMfa
  | EnableMfaSuccess
  | EnableMfaFailure
  | ActivateMfa
  | ActivateMfaSuccess
  | ActivateMfaFailure
  | DeactivateMfa
  | DeactivateMfaSuccess
  | DeactivateMfaFailure
  | ResetForm;
