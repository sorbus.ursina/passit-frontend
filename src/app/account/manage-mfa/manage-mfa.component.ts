import { Component, Input, Output, EventEmitter } from "@angular/core";
import * as QRCode from "qrcode";
import { FormGroupState } from "ngrx-forms";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { ActivateMFAStep } from "./manage-mfa.interfaces";
import { IEnableMfaForm } from "./manage-mfa.reducer";
import { IU2fKey } from "../manage-u2f/manage-u2f.interfaces";

@Component({
  selector: "app-manage-mfa",
  templateUrl: "./manage-mfa.component.html",
  styleUrls: [
    "./manage-mfa.component.scss",
    "../manage-backup-code/manage-backup-code.component.scss",
    "../account.component.scss",
    "../../../styles/_utility.scss"
  ]
})
export class ManageMfaComponent {
  private _uri: string;
  @Input() step: ActivateMFAStep;
  @Input() form: FormGroupState<IEnableMfaForm>;
  @Input() errors: string[] | null;
  @Input() mfaRequired: boolean;
  @Input() u2fList: IU2fKey[];
  @Input()
  set uri(value: string) {
    this._uri = value;
    if (value) {
      QRCode.toDataURL(value).then(dataUrl => (this.qrCode = dataUrl));
    } else {
      this.qrCode = null;
    }
  }
  get uri() {
    return this._uri;
  }
  qrCode: string | null;

  @Output() generateMfa = new EventEmitter();
  @Output() verifyMfa = new EventEmitter();
  @Output() forwardStep = new EventEmitter();
  @Output() deactivateMfa = new EventEmitter();

  constructor(private _sanitizer: DomSanitizer) {}

  getURI(): SafeUrl {
    return this._sanitizer.bypassSecurityTrustUrl(this._uri);
  }
}
