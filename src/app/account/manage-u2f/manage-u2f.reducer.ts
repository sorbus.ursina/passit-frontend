import {
  FormGroupState,
  validate,
  updateGroup,
  createFormGroupState,
  createFormStateReducerWithUpdate
} from "ngrx-forms";
import { required } from "ngrx-forms/validation";
import { ManageU2fActions, ManageU2fActionTypes } from "./manage-u2f.actions";
import { IU2fKey } from "./manage-u2f.interfaces";

export const FORM_ID = "Enable U2F Form";

export interface IEnableU2fForm {
  name: string;
}

export interface IManageU2fState {
  form: FormGroupState<IEnableU2fForm>;
  u2fIsBegun: boolean;
  awaitingResponse: boolean;
  errorMessage: string[] | null;
  regSuccess: boolean;
  u2fList: IU2fKey[];
}

const validateAndUpdateFormState = updateGroup<IEnableU2fForm>({
  name: validate(required)
});

export const initialFormState = validateAndUpdateFormState(
  createFormGroupState<IEnableU2fForm>(FORM_ID, {
    name: ""
  })
);

export const initialState: IManageU2fState = {
  form: initialFormState,
  u2fIsBegun: false,
  awaitingResponse: false,
  errorMessage: null,
  regSuccess: false,
  u2fList: []
};

export const formReducer = createFormStateReducerWithUpdate<IEnableU2fForm>(
  validateAndUpdateFormState
);

export function reducer(
  state = initialState,
  action: ManageU2fActions
): IManageU2fState {
  const form = formReducer(state.form, action);
  state = { ...state, form };

  switch (action.type) {
    case ManageU2fActionTypes.BEGIN_U2F_REG:
      return {
        ...state,
        u2fIsBegun: true
      };

    case ManageU2fActionTypes.ENABLE_U2F:
      if (state.form.isValid) {
        return {
          ...state,
          awaitingResponse: true
        };
      } else {
        return {
          ...state
        };
      }

    case ManageU2fActionTypes.ENABLE_U2F_SUCCESS:
      return {
        ...state,
        awaitingResponse: false,
        errorMessage: null,
        regSuccess: true,
        u2fIsBegun: false,
        form: initialFormState,
        u2fList: [...state.u2fList, action.payload],
      };

    case ManageU2fActionTypes.ENABLE_U2F_FAILURE:
      if (!state.form.isValid) {
        return {
          ...state,
          awaitingResponse: false,
          errorMessage: ["Device name is required."]
        };
      } else {
        return {
          ...state,
          awaitingResponse: false,
          errorMessage: action.payload
        };
      }

    case ManageU2fActionTypes.U2F_RESET:
      return initialState;

    case ManageU2fActionTypes.FETCH_U2F_LIST_SUCCESS:
      return {
        ...state,
        u2fList: action.payload
      };

    case ManageU2fActionTypes.FETCH_U2F_LIST_FAILURE:
      return {
        ...state,
        errorMessage: action.payload
      };

    case ManageU2fActionTypes.DELETE_KEY_SUCCESS:
      return {
        ...state,
        u2fList: state.u2fList.filter(key => key.id !== action.payload)
      };

    case ManageU2fActionTypes.DELETE_KEY_FAILURE:
    return {
      ...state,
      errorMessage: action.payload
    };

  }
  return state;
}

export const getForm = (state: IManageU2fState) => state.form;
export const getRegStatus = (state: IManageU2fState) => state.u2fIsBegun;
export const getWaitingStatus = (state: IManageU2fState) => state.awaitingResponse;
export const getErrorMessage = (state: IManageU2fState) => state.errorMessage;
export const getRegSuccess = (state: IManageU2fState) => state.regSuccess;
export const getU2fList = (state: IManageU2fState) => state.u2fList;
