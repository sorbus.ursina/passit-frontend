export interface IU2fKey {
    id: number;
    key_type: string;
    name: string;
    last_used: Date;
  }
