import { Action } from "@ngrx/store";
import { IU2fKey } from "./manage-u2f.interfaces";

export enum ManageU2fActionTypes {
    BEGIN_U2F_REG = "[ENABLE U2F] Begin U2F Registration",
    ENABLE_U2F = "[ENABLE U2F] Enable",
    ENABLE_U2F_SUCCESS = "[ENABLE U2F] Enable Success",
    ENABLE_U2F_FAILURE = "[ENABLE U2F] Enable Failure",
    U2F_RESET = "[ENABLE U2F] Reset U2F registration state",
    FETCH_U2F_LIST = "[MANAGE U2F] Fetch U2F list",
    FETCH_U2F_LIST_SUCCESS = "[MANAGE U2F] Fetch U2F list success",
    FETCH_U2F_LIST_FAILURE = "[MANAGE U2F] Fetch U2F list failure",
    DELETE_KEY = "[MANAGE U2F] Delete U2F key",
    DELETE_KEY_SUCCESS = "[MANAGE U2F] Delete U2F key success",
    DELETE_KEY_FAILURE = "[MANAGE U2F] Delete U2F key failure"
}

export class BeginU2fReg implements Action {
    readonly type = ManageU2fActionTypes.BEGIN_U2F_REG;
  }

export class EnableU2f implements Action {
  readonly type = ManageU2fActionTypes.ENABLE_U2F;
}

export class EnableU2fSuccess implements Action {
  readonly type = ManageU2fActionTypes.ENABLE_U2F_SUCCESS;

  constructor(public payload: IU2fKey) {}
}

export class EnableU2fFailure implements Action {
  readonly type = ManageU2fActionTypes.ENABLE_U2F_FAILURE;

  constructor(public payload: string[]) {}
}

export class U2fReset implements Action {
  readonly type = ManageU2fActionTypes.U2F_RESET;
}

export class FetchU2fList implements Action {
  readonly type = ManageU2fActionTypes.FETCH_U2F_LIST;
}

export class FetchU2fListSuccess implements Action {
  readonly type = ManageU2fActionTypes.FETCH_U2F_LIST_SUCCESS;

  constructor(public payload: IU2fKey[]) {}
}

export class FetchU2fListFailure implements Action {
  readonly type = ManageU2fActionTypes.FETCH_U2F_LIST_FAILURE;

  constructor(public payload: string[]) {}
}

export class DeleteKey implements Action {
  readonly type = ManageU2fActionTypes.DELETE_KEY;

  constructor(public payload: number) {}
}

export class DeleteKeySuccess implements Action {
  readonly type = ManageU2fActionTypes.DELETE_KEY_SUCCESS;

  constructor(public payload: number) {}
}

export class DeleteKeyFailure implements Action {
  readonly type = ManageU2fActionTypes.DELETE_KEY_FAILURE;

  constructor(public payload: string[]) {}
}

export type ManageU2fActions =
| BeginU2fReg
| EnableU2f
| EnableU2fSuccess
| EnableU2fFailure
| U2fReset
| FetchU2fList
| FetchU2fListSuccess
| FetchU2fListFailure
| DeleteKey
| DeleteKeySuccess
| DeleteKeyFailure;
