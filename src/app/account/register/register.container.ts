import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Store, select } from "@ngrx/store";

import * as fromRoot from "../../app.reducers";
import * as App from "../../app.actions";
import * as fromAccount from "../account.reducer";
import { UserService } from "../user";
import * as Register from "./register.actions";
import { SetValueAction, MarkAsSubmittedAction } from "ngrx-forms";
import { IS_EXTENSION } from "../../constants";
import { FORM_ID } from "./register.reducer";

@Component({
  providers: [UserService],
  template: `
    <register-component
      [form]="form$ | async"
      [urlForm]="urlForm$ | async"
      [stageValue]="stageValue$ | async"
      [backupCode]="backupCode$ | async"
      [isEmailTaken]="isEmailTaken$ | async"
      [isUrlValid]="isUrlValid$ | async"
      [urlDisplayName]="urlDisplayName$ | async"
      [showUrl]="showUrl$ | async"
      [errorMessage]="errorMessage$ | async"
      [hasSubmitStarted]="hasSubmitStarted$ | async"
      [hasSubmitFinished]="hasSubmitFinished$ | async"
      [isExtension]="isExtension"
      (register)="register()"
      (goToLogin)="goToLogin()"
      (incrementStage)="incrementStage()"
      (switchStage)="switchStage($event)"
      (checkEmail)="checkEmail()"
      (checkUrl)="checkUrl()"
      (displayUrlInput)="displayUrlInput()"
      (hideUrlInput)="hideUrlInput()"
      (toggleShowConfirm)="toggleConfirm()"
      (markAsSubmitted)="markAsSubmitted()"
      (registrationFinished)="registrationFinished()"
    ></register-component>
  `
})
export class RegisterContainer implements OnDestroy, OnInit {
  form$ = this.store.pipe(select(fromAccount.getRegisterForm));
  urlForm$ = this.store.pipe(select(fromAccount.getUrlForm));
  errorMessage$ = this.store.pipe(select(fromAccount.getRegisterErrorMessage));
  hasSubmitStarted$ = this.store.pipe(select(fromAccount.getHasSubmitStarted));
  hasSubmitFinished$ = this.store.pipe(
    select(fromAccount.getHasSubmitFinished)
  );
  stageValue$ = this.store.pipe(select(fromAccount.getStage));
  isEmailTaken$ = this.store.pipe(select(fromAccount.getRegisterIsEmailTaken));
  isUrlValid$ = this.store.pipe(select(fromAccount.getisUrlValid));
  urlDisplayName$ = this.store.pipe(select(fromAccount.getUrlDisplayName));
  showUrl$ = this.store.pipe(select(fromAccount.getShowUrl));
  backupCode$ = this.store.pipe(select(fromAccount.getRegisterBackupCode));
  isExtension = IS_EXTENSION;

  showConfirm = true;

  constructor(private store: Store<fromRoot.IState>, private router: Router) {
    this.form$.subscribe(
      form => (this.showConfirm = form.controls.showConfirm.value)
    );
  }

  goToLogin(email?: string) {
    this.store.dispatch(new App.LoginRedirect(email));
  }

  register() {
    this.store.dispatch(Register.register());
  }

  markAsSubmitted() {
    this.store.dispatch(new MarkAsSubmittedAction(FORM_ID));
  }

  incrementStage() {
    this.store.dispatch(Register.incrementStage());
  }

  checkEmail() {
    this.store.dispatch(Register.checkEmail());
  }

  checkUrl() {
    this.store.dispatch(Register.checkUrl());
  }

  switchStage(value: number) {
    this.store.dispatch(Register.switchStage({ payload: value }));
  }

  toggleConfirm() {
    this.store.dispatch(
      new SetValueAction(FORM_ID + ".showConfirm", !this.showConfirm)
    );
  }

  displayUrlInput() {
    this.store.dispatch(Register.displayUrl());
  }

  hideUrlInput() {
    this.store.dispatch(Register.hideUrl());
  }

  registrationFinished() {
    if (IS_EXTENSION) {
      this.router.navigate(["/popup"]);
    } else {
      this.router.navigate(["/list"]);
    }
  }

  ngOnDestroy() {
    this.store.dispatch(Register.registerClear());
  }

  ngOnInit() {
    // A default server must be set initially when using the ext.
    if (IS_EXTENSION) {
      this.store.dispatch(Register.checkUrl());
    }
  }
}
