import {
  createFeatureSelector,
  createSelector,
  ActionReducerMap
} from "@ngrx/store";
import * as fromConfirmEmail from "./confirm-email/confirm-email.reducer";
import * as fromRegister from "./register/register.reducer";
import * as fromChangePassword from "./change-password/change-password.reducer";
import * as fromDeleteAccount from "./delete/delete.reducer";
import * as fromErrorReporting from "./error-reporting/error-reporting.reducer";
import * as fromResetPassword from "./reset-password/reset-password.reducer";
import * as fromResetPasswordVerify from "./reset-password/reset-password-verify/reset-password-verify.reducer";
import * as fromSetPassword from "./reset-password/set-password/set-password.reducer";
import * as fromManageBackupCode from "./manage-backup-code/manage-backup-code.reducer";
import * as fromManageMfa from "./manage-mfa/manage-mfa.reducer";
import * as fromManageU2f from "./manage-u2f/manage-u2f.reducer";
import * as fromRoot from "../app.reducers";

export interface IAccountState {
  register: fromRegister.IRegisterState;
  confirmEmail: fromConfirmEmail.IConfirmEmailState;
  changePassword: fromChangePassword.IChangePasswordState;
  deleteAccount: fromDeleteAccount.IDeleteAccountState;
  errorReporting: fromErrorReporting.IState;
  resetPassword: fromResetPassword.IResetPasswordState;
  resetPasswordVerify: fromResetPasswordVerify.IResetPasswordVerifyState;
  manageBackupCode: fromManageBackupCode.IState;
  setPassword: fromSetPassword.ISetPasswordState;
  enableMfa: fromManageMfa.IEnbleMfaState;
  manageU2f: fromManageU2f.IManageU2fState;
}

export const initialState: IAccountState = {
  register: fromRegister.initialState,
  confirmEmail: fromConfirmEmail.initialState,
  changePassword: fromChangePassword.initialState,
  deleteAccount: fromDeleteAccount.initialState,
  errorReporting: fromErrorReporting.initialState,
  resetPassword: fromResetPassword.initialState,
  resetPasswordVerify: fromResetPasswordVerify.initialState,
  manageBackupCode: fromManageBackupCode.initialState,
  setPassword: fromSetPassword.initialState,
  enableMfa: fromManageMfa.initialState,
  manageU2f: fromManageU2f.initialState
};

export const reducers: ActionReducerMap<IAccountState> = {
  register: fromRegister.reducer,
  confirmEmail: fromConfirmEmail.reducer,
  changePassword: fromChangePassword.reducer,
  deleteAccount: fromDeleteAccount.reducer,
  errorReporting: fromErrorReporting.reducer,
  resetPassword: fromResetPassword.reducer,
  resetPasswordVerify: fromResetPasswordVerify.reducer,
  setPassword: fromSetPassword.reducer,
  manageBackupCode: fromManageBackupCode.reducer,
  enableMfa: fromManageMfa.reducer,
  manageU2f: fromManageU2f.reducer
};

export interface IState extends fromRoot.IState {
  account: IAccountState;
}

export const selectAccountState = createFeatureSelector<IState, IAccountState>(
  "account"
);

export const selectRegisterState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.register
);

export const getRegisterForm = createSelector(
  selectRegisterState,
  fromRegister.getForm
);

export const getUrlForm = createSelector(
  selectRegisterState,
  fromRegister.getUrlForm
);

export const getHasSubmitStarted = createSelector(
  selectRegisterState,
  fromRegister.getHasSubmitStarted
);

export const getHasSubmitFinished = createSelector(
  selectRegisterState,
  fromRegister.getHasSubmitFinished
);

export const getRegisterErrorMessage = createSelector(
  selectRegisterState,
  fromRegister.getErrorMessage
);

export const getRegisterBackupCode = createSelector(
  selectRegisterState,
  fromRegister.getBackupCode
);

export const getStage = createSelector(
  selectRegisterState,
  fromRegister.getStage
);

export const getUrlDisplayName = createSelector(
  selectRegisterState,
  fromRegister.getUrlDisplayName
);

export const getShowUrl = createSelector(
  selectRegisterState,
  fromRegister.getShowUrl
);

export const getRegisterIsEmailTaken = createSelector(
  selectRegisterState,
  fromRegister.getIsEmailTaken
);

export const getisUrlValid = createSelector(
  selectRegisterState,
  fromRegister.getIsUrlValid
);

export const selectConfirmEmailState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.confirmEmail
);

export const getConfirmEmailMessage = createSelector(
  selectConfirmEmailState,
  fromConfirmEmail.getResetCodeMessage
);
export const getConfirmIsVerified = createSelector(
  selectConfirmEmailState,
  fromConfirmEmail.getIsVerified
);
export const getConfirmHasStarted = createSelector(
  selectConfirmEmailState,
  fromConfirmEmail.getHasStarted
);
export const getConfirmHasFinished = createSelector(
  selectConfirmEmailState,
  fromConfirmEmail.getHasFinished
);
export const getConfirmErrorMessage = createSelector(
  selectConfirmEmailState,
  fromConfirmEmail.getErrorMessage
);

export const selectChangePasswordForm = createSelector(
  selectAccountState,
  (state: IAccountState) => state.changePassword
);
export const changePassword = createSelector(
  selectChangePasswordForm,
  fromChangePassword.getForm
);
export const changePasswordHasStarted = createSelector(
  selectChangePasswordForm,
  fromChangePassword.getHasStarted
);
export const changePasswordHasFinished = createSelector(
  selectChangePasswordForm,
  fromChangePassword.getHasFinished
);
export const selectChangePasswordNewBackupCode = createSelector(
  selectChangePasswordForm,
  fromChangePassword.getNewBackupCode
);
export const changePasswordErrorMessage = createSelector(
  selectChangePasswordForm,
  fromChangePassword.getErrorMessage
);

export const selectDeleteAccountState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.deleteAccount
);
export const getDeleteErrorMessage = createSelector(
  selectDeleteAccountState,
  fromDeleteAccount.getErrorMessage
);
export const getDeleteForm = createSelector(
  selectDeleteAccountState,
  fromDeleteAccount.getForm
);
export const getDeleteHasStarted = createSelector(
  selectDeleteAccountState,
  fromDeleteAccount.getHasStarted
);

export const selectErrorReportingState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.errorReporting
);
export const getErrorReportingForm = createSelector(
  selectErrorReportingState,
  fromErrorReporting.getForm
);
export const getErrorReportingHasFinished = createSelector(
  selectErrorReportingState,
  fromErrorReporting.getHasFinished
);
export const getErrorReportingHasStarted = createSelector(
  selectErrorReportingState,
  fromErrorReporting.getHasStarted
);

export const selectResetPasswordState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.resetPassword
);
export const getResetPasswordForm = createSelector(
  selectResetPasswordState,
  fromResetPassword.getForm
);
export const getResetPasswordHasStarted = createSelector(
  selectResetPasswordState,
  fromResetPassword.getHasStarted
);
export const getResetPasswordHasFinished = createSelector(
  selectResetPasswordState,
  fromResetPassword.getHasFinished
);
export const getResetPasswordErrorMessage = createSelector(
  selectResetPasswordState,
  fromResetPassword.getErrorMessage
);

export const getEmailDisplayName = createSelector(
  selectResetPasswordState,
  fromResetPassword.getEmailDisplayName
);

export const selectManageBackupCodeForm = createSelector(
  selectAccountState,
  (state: IAccountState) => state.manageBackupCode
);

export const manageBackupCode = createSelector(
  selectManageBackupCodeForm,
  fromManageBackupCode.getForm
);

export const manageBackupCodeHasStarted = createSelector(
  selectManageBackupCodeForm,
  fromManageBackupCode.getHasStarted
);

export const manageBackupCodeHasFinished = createSelector(
  selectManageBackupCodeForm,
  fromManageBackupCode.getHasFinished
);

export const selectManageBackupCodeNewBackupCode = createSelector(
  selectManageBackupCodeForm,
  fromManageBackupCode.getNewBackupCode
);

export const manageBackupCodeErrorMessage = createSelector(
  selectManageBackupCodeForm,
  fromManageBackupCode.getErrorMessage
);

export const selectResetPasswordVerifyState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.resetPasswordVerify
);
export const getResetPasswordVerifyForm = createSelector(
  selectResetPasswordVerifyState,
  fromResetPasswordVerify.getForm
);
export const getResetPasswordVerifyHasStarted = createSelector(
  selectResetPasswordVerifyState,
  fromResetPasswordVerify.getHasStarted
);
export const getResetPasswordVerifyErrorMessage = createSelector(
  selectResetPasswordVerifyState,
  fromResetPasswordVerify.getErrorMessage
);
export const getResetPasswordVerifyEmailAndCode = createSelector(
  selectResetPasswordVerifyState,
  fromResetPasswordVerify.getEmailAndCode
);

export const selectSetPasswordState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.setPassword
);
export const getSetPasswordForm = createSelector(
  selectSetPasswordState,
  fromSetPassword.getForm
);
export const getSetPasswordHasStarted = createSelector(
  selectSetPasswordState,
  fromSetPassword.getHasStarted
);
export const getSetPasswordBackupCode = createSelector(
  selectSetPasswordState,
  fromSetPassword.getBackupCode
);

export const selectManageMfaState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.enableMfa
);
export const getEnableMfaForm = createSelector(
  selectManageMfaState,
  fromManageMfa.getForm
);
export const getMfaStep = createSelector(
  selectManageMfaState,
  fromManageMfa.getStep
);
export const getMfaErrorMessage = createSelector(
  selectManageMfaState,
  fromManageMfa.getErrorMessage
);
export const getMfaProvisioningURI = createSelector(
  selectManageMfaState,
  fromManageMfa.getProvisioningURI
);
export const getMfaId = createSelector(
  selectManageMfaState,
  fromManageMfa.getMFAId
);

export const selectManageU2fState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.manageU2f
);
export const getU2fRegStatus = createSelector(
  selectManageU2fState,
  fromManageU2f.getRegStatus
);
export const getWaitingStatus = createSelector(
  selectManageU2fState,
  fromManageU2f.getWaitingStatus
);
export const getRegSuccess = createSelector(
  selectManageU2fState,
  fromManageU2f.getRegSuccess
);
export const getU2fErrorMessage = createSelector(
  selectManageU2fState,
  fromManageU2f.getErrorMessage
);

export const getEnableU2fForm = createSelector(
  selectManageU2fState,
  fromManageU2f.getForm
);

export const getU2fList = createSelector(
  selectManageU2fState,
  fromManageU2f.getU2fList
);
