import {
  createFormGroupState,
  FormGroupState,
  validate,
  createFormStateReducerWithUpdate,
  updateGroup
} from "ngrx-forms";
import { oldPasswordValidators } from "../constants";
import {
  DeleteAccountActions,
  DeleteAccountActionTypes
} from "./delete.actions";
import { IBaseFormState } from "~/app/utils/interfaces";
export const FORM_ID = "Delete Account Form";

export interface IDeleteAccountForm {
  password: string;
}

export interface IDeleteAccountState extends IBaseFormState {
  form: FormGroupState<IDeleteAccountForm>;
  errorMessage: string | null;
}

const initialFormState = createFormGroupState<IDeleteAccountForm>(FORM_ID, {
  password: ""
});

export const initialState = {
  form: initialFormState,
  hasStarted: false,
  hasFinished: false,
  errorMessage: null
};

export const validateAndUpdateFormState = updateGroup<IDeleteAccountForm>({
  password: validate<string>(oldPasswordValidators)
});

export const formReducer = createFormStateReducerWithUpdate<IDeleteAccountForm>(
  validateAndUpdateFormState
);

export function reducer(
  state = initialState,
  action: DeleteAccountActions
): IDeleteAccountState {
  const form = formReducer(state.form, action);
  if (form !== state.form) {
    state = { ...state, form };
  }
  switch (action.type) {
    case DeleteAccountActionTypes.DELETE_ACCOUNT_FAILURE:
      return {
        ...state,
        hasStarted: false,
        errorMessage: action.payload
      };

    case DeleteAccountActionTypes.DELETE_ACCOUNT:
      return {
        ...state,
        hasStarted: true,
        hasFinished: false
      };

    case DeleteAccountActionTypes.DELETE_ACCOUNT_SUCCESS:
      return {
        ...state,
        hasFinished: true
      };

    case DeleteAccountActionTypes.RESET_STATE:
      return initialState;

    case DeleteAccountActionTypes.RESET_ERROR_MESSAGE:
      return {
        ...state,
        errorMessage: null
      };

    default:
      return state;
  }
}

export const getForm = (state: IDeleteAccountState) => state.form;
export const getHasStarted = (state: IDeleteAccountState) => state.hasStarted;
export const getErrorMessage = (state: IDeleteAccountState) =>
  state.errorMessage;
