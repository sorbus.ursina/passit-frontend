export interface IAuthStore {
  publicKey: string;
  privateKey: string;
  userId: number;
  email: string;
  userToken: string;
  rememberMe: boolean;
  optInErrorReporting: boolean;
  mfaRequired: boolean;
  validAuth: string[];
}

export interface IResetPasswordVerifyResponse {
  public_key: string;
  private_key_backup: string;
  server_public_key: string;
}

export interface IAuthResponse {
  token: string;
}
