import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { StoreModule, select } from "@ngrx/store";

import { User } from "./user";
import { UserService } from "./user.service";

import { Store } from "@ngrx/store";
import { IState, getUrl } from "../../app.reducers";
import { NgPassitSDK } from "../../ngsdk/sdk";
import { reducers } from "../account.reducer";

const sdkStub = {
  is_username_available: () => {
    return Promise.resolve(true);
  }
};

describe("Service: UserService", () => {
  let service: UserService;
  let store: Store<IState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot({}),
        StoreModule.forFeature("account", reducers)
      ],
      providers: [{ provide: NgPassitSDK, useValue: sdkStub }, UserService]
    });
    service = TestBed.inject(UserService);
    store = TestBed.inject(Store);
  });

  it("Can set up SDK", (done: any) => {
    const user: User = {
      email: "test@example.com",
      password: "mypass"
    };

    service.checkUsername(user.email).subscribe(isAvail => {
      expect(isAvail.isAvailable).toBe(true);
      done();
    });
  });

  it("Can check and set url", () => {
    const correctUrl = "https://api.passit.io/api/";
    // mockIt(mockBackend, {
    //   body: "{}",
    //   url: correctUrl,
    // });
    const url = "passit.io"; // The service should prefix https and api
    service.checkAndSetUrl(url).subscribe(() => {
      store.pipe(select(getUrl)).subscribe(storeUrl => {
        expect(storeUrl).toBe(correctUrl);
      });
    });
  });
});
