import { Injectable } from "@angular/core";
import { Action, Store, select } from "@ngrx/store";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { Observable, of } from "rxjs";
import { withLatestFrom, map, catchError, exhaustMap } from "rxjs/operators";

import {
  ManageBackupCodeActionTypes,
  SubmitForm,
  SubmitFormSuccess,
  SubmitFormFailure
} from "./manage-backup-code.actions";
import * as fromAccount from "../account.reducer";
import { UserService } from "../user";


@Injectable()
export class ManageBackupCodeEffects {
  constructor(
    private actions$: Actions,
    private userService: UserService,
    private store: Store<any>
  ) {}

  @Effect()
  getBackupCode$: Observable<Action> = this.actions$.pipe(
    ofType<SubmitForm>(ManageBackupCodeActionTypes.SUBMIT_FORM),
    withLatestFrom(this.store.pipe(select(fromAccount.manageBackupCode))),
    map(([action, form]) => form.value.oldPassword),
    exhaustMap(currentPassword =>
      this.userService.makeAndSetBackupCode(currentPassword).pipe(
        map(backupCode => new SubmitFormSuccess(backupCode)),
        catchError((err) => {
          if (err.status === 400) {
            return of(new SubmitFormFailure(["Incorrect password."]));
          } else {
            return of(new SubmitFormFailure(["Unexpected error."]));
          }
        })
      )
    )
  );
}
