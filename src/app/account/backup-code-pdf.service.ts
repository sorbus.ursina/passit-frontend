import { Injectable } from "@angular/core";
import { jsPDF } from "jspdf";
import * as QRCode from "qrcode";
import Canvg from "canvg";

@Injectable({
  providedIn: "root",
})
export class BackupCodePdfService {
  /** Convert svg to canvas which is compatible with jspdf
   * Optional width and height values are useful to resize the svg
   * Return canvas contains a png.
   */
  private async svgToCanvas(svg: any, width?: number, height?: number) {
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");
    if (width) {
      canvas.width = width;
    }
    if (height) {
      canvas.height = height;
    }
    const v = await Canvg.from(ctx!, svg);
    await v.render();
    return canvas;
  }

  /* Wrapper around svgToCanvas with canvg height width based resizing */
  private async svgToPdf(
    pdf: jsPDF,
    svg: any,
    x: number,
    y: number,
    width?: number,
    height?: number
  ) {
    const imageData = await this.svgToCanvas(svg, width, height);
    pdf.addImage(imageData, "PNG", x, y, 2, 2, undefined, "FAST" as any, 0);
  }

  /** Format current date for styling */
  private getDate() {
    const dateTodayoptions = {
      month: "long",
      day: "numeric",
      year: "numeric",
    } as const;
    const timeTodayoptions = {
      hour: "numeric",
      minute: "2-digit",
      hour12: true,
    } as const;
    const date = new Date(),
      locale = "en-us",
      dateToday = date
        .toLocaleString(locale, dateTodayoptions)
        /** Microsoft Edge outputs the .toLocalString weirdly
         * .replace(regex) fixes it: https://github.com/MrRio/jsPDF/issues/937 */
        .replace(/[\u200E]/g, ""),
      timeToday = date
        .toLocaleString(locale, timeTodayoptions)
        .replace(/[\u200E]/g, "");

    return "Created on " + dateToday + " at " + timeToday;
  }

  /** Set pdf document text to header1 styling */
  private header1(doc: jsPDF) {
    doc.setFont("helvetica", "bold").setFontSize(24).setTextColor(25, 22, 25);
  }

  /** Set pdf document text to header2 styling */
  private header2(doc: jsPDF) {
    doc.setFont("helvetica", "bold").setFontSize(18).setTextColor(0, 146, 168);
  }

  /** Set pdf document text to paragraph styling */
  private paragraph(doc: jsPDF) {
    doc
      .setFont("helvetica", "normal")
      .setFontSize(13.5)
      .setTextColor(25, 22, 25);
  }

  /** Set pdf document text to aside styling */
  private aside(doc: jsPDF) {
    doc.setFont("helvetica", "italic").setFontSize(12).setTextColor(25, 22, 25);
  }

  /** Set pdf document text to qrText styling */
  private qrText(doc: jsPDF) {
    doc.setFont("courier", "normal").setFontSize(17).setTextColor(0, 0, 0);
  }

  /**
   * Adds text to a pdf document.
   * @param doc: jsPDF document
   * @param style: function to add style to the doc
   * @param docText: Array of text to add to doc, we use this instead of textwrap
   * @param margin: determines the placement on the x axis (in inches)
   * @param yAxis: determines the placement on the y axis (in inches)
   * @param lineHeight: determines the spacing between each line (in inches)
   */
  private docAddText(
    doc: jsPDF,
    style: (doc: jsPDF) => void,
    docText: string[],
    margin: number,
    yAxis: number,
    lineHeight: number
  ) {
    docText.forEach((textString, iText) => {
      style(doc);
      doc.text(textString, margin, yAxis + iText * lineHeight);
    });
  }

  /** Get Passit instance's domain */
  private getDomain() {
    if (window) {
      return window.location.hostname;
    }
    return "app.passit.io";
  }

  /**
   * Downloads PDF containing backup code string and matching QR Code
   * @param code: 32 character backupcode string
   */
  download(code: string) {
    // Places elements on Y axis
    const logoYAxis = 0.75, // y-coordinate of logo's upper left corner
      mainHeaderYAxis = 2.1,
      savedHeaderYAxis = mainHeaderYAxis + 0.6,
      backupSentence1YAxis = savedHeaderYAxis + 0.4,
      backupSentence2YAxis = backupSentence1YAxis + 0.7,
      backupSentence3YAxis = backupSentence2YAxis + 0.7,
      recoveryHeaderYAxis = 5.4,
      recoveryParagraphYAxis = recoveryHeaderYAxis + 0.4,
      rectangleYAxis = 7.1,
      issuesParagraphYAxis = rectangleYAxis + 0.9,
      dateTimeStampYAxis = issuesParagraphYAxis + 0.7,
      qrCodeYAxis = rectangleYAxis + 0.15,
      recoveryCodeYAxis = rectangleYAxis + 2.45;

    // Placing an svg into a pdf is not easy
    // https://stackoverflow.com/a/35788928/443457
    QRCode.toString(code).then((image) => {
      const doc = new jsPDF({
        format: "letter",
        unit: "in",
      });

      // Styles
      const defaultMargin = 1;
      const defaultLineHeight = 0.3;
      // tslint:disable-next-line:max-line-length
      const logo =
        // tslint:disable-next-line: max-line-length
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA6sAAAF0BAMAAAA3O7JlAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAYUExURQCSqP///0Guvr/k6uT09p3V3m7BziKgtHH1Be8AABAeSURBVHja7J3LY9pIEsZlnlcDAq6Qh/cKduJcUR6eKyTZ5Ipsj32FiWf2318nGwciVXd91ZJnsvRXV1uN1L+uR1dVS0lCoVAoFAqFQqFQKBQKhUKhUCgUCoVCoVAoFAqFQqFQKBQKhUKhUCgUCoVCeSQ56lH+T4VYiZVCrBRipRArhViJlViJlUKsFGKlECuFWImVWImVQqwUYqUQK4VYiZVCrBRipRArhViJlViJlUKsFGKlECuFWImVWImVQqwUYqUQK4VYiZVYiZVCrBRipRArhViJlUKsFGKlECuFWImVWImVQqwUYqUQK4VYiZVYiZVCrBRipRArhViJlUKsFGKlECuFWCnESqwUYqUQK4VYKcRKrMRKrBRi/SXkzctPNzdPntzcfHgRPkh6dj/I5f0gv59lxPrPM/202Xus7vMgsunL2/3JefKBWP9ReXdberKTuRnqTyvjmzSuDhrr6RNATu7N35n57oqjBJi+wa30aF0jkvONNMpfyuoYFO/f92xWyR8Z6yyB5eSDCcygeH1upnoxcdzKn4Y7SdeOQbofvdcNi/+Pzjwi018H61c1MUznqHj11kr13H0nHdgQDybuUa6I9btL+ghDWRSvbRqpnnpvZF6dapI8J9aHmUAVdlnSsBqp3luOeXWqSfKFWB9CDZBrOU4xuebX2n10supUk+SaWA3zeR+plC9cWaKlOu7DGS0hgVxsWDGuI+OzFNRsA9zHF22UV0gYmBHrw/4CGHhcvqyFY11D95FX1niPy48Pa/IsIGKyxEyvsdtoeM1GusFGuSZW3E1KwUqtJvir3NXxaA4zHCNW1b2m0lVoDgG/qblhg2JcHDFi9euJa1LB9CEOJGlXdc/uxREl1m5mjpjgmGlZhzcYGQZpEiuYCpwZdStMWT1Dri2jzIkVc5TirDYe4ZbmNawN0YxEirVpjpiwmClNKhMxP5fkUiLF6vWuw6D8wTf5rY7bQPesnmmOFKs3GO6H39TEeBs5HLHZUk2xYu1Yiq1wyXVYizNYW0eZEyuQanJMKxAzLcy3kVX2z6LpiRZry1JsRUuuZj2TrPC4BtMTLdYG3p4Gx0xD+200qyU0XFY4WqwBKR615GrXMykW3tQw0fFibZkdpBozBehZ2QQEqHw5XRUv1o4ZjlpyndSxusYhD5MRqxYBbcxXaE7ZtLpCVL6k8xFjzc1wVuYOqIDVNanDo0SMtWWGMw3btf5xc3NzuQFXV+obZILqfMRYO+ZwthUQMTUezoi8vIUGlVfVwwnKN+8dATWxio8K+baOPWLa7zd/NwGiWGlVNfaOxcrn8Io714PC2j4T5dPEUmmbmBeCx3z+3A0sdnQ31KVa6L36DEy1F6vrgGPJ7v8iByGdRlI+JDo1V0znxhxTUb1FrpliLEoHdj7rpnxoWpAOIqtegPydWHvDDeoqRwHBs+vK8gkq6T5yxViU+pqlxdGOEqvYJN82JwBbttThHXQfU/+0dKA9WCNOrNKRloY5DGvb9jdzqMjT8hPbYk0YWZxYpU6SzFpba5ji8g7mga/9WDPoYf6KFKu0wufmGvbcsm3dIv9WeOPACNtUFR/mqhepEZbUNTeXTHNLDX0FBMxXWtzVAjZTHVu99aCwCl5zai6ZHlmwZur/NV6o7XG5/jBXxha1w8I6RALbhbmZwbk1cTniHTnhVRZ90OwPPaoaF9Zyxa1p7kZqGLB2lGyU+OKZMVoMnLhVNTKsM2C/onWcZDjWpv8+5LfOjFEeC7eqRob1WI8y1Y6TvAas/cT9jqixUpr5Oba6gh3OAWMd6Sa1X6F6AWO935ueZGCqquEpLHjexhYT1kGiqsJCw9qsAWvvX1dwBrLrDryvLOHhAWNN9UJbMaXQXeMl1wmcacwMiWX30/SI1REPZdp/dJZ4zGRYAW7pG9viiFUoemWamW4u8PTh2pJAhrGuiFXDutYYlYKqoz4eM63xFWAI66bEqmFdahNf0s18iJdcl3UQGVryWsSKYS3/Q4q3qc2SGpxriYf2Vhpi1bEKzncCT9DC1iODRut3xGr8/yLWVMhX4B5znNShrkkt6kqsvnilLblbOIj1vccZj9aD1JVG2KdvRwKsFjyVQZomlJBWxFoJ60zQzCHcpiadyeqYuQotco2MWE2akCkGcC77W8MT/WmdlkUtiyPuLJMSMXVdrGH7aecq1pD+yogV/8muMhcdl2WG7aedq3y81so16sJcQ4mYWq44Ct3hBCBxdLTiX7qKDqvWgyvr5QhO57mO7zRM8+Popup+JFZQm9pIrh6PmZyt46avQDor+VfEivm+gt5tZNerlvM0RUv0D3MCOm8aJCasa39mYeiw0XjM5GmZwRXWd1wE/kZeRFjL87X1by2+D3cMF9y8Z2M7qHP0tSqjH7WMCOtYqbAsHPjwmEnpMgYV1t8mhylsRFjXSrJ17fjzoELJNcQ5Kr3KkMLGg3WgtehvXMkKPGbSXriFediJMsoVsfo0qevH3kErBHvuWzvrAeUm1Bf+61ofDVZBWTt+TWtqTtduhbHchP7uxO41sTpUrvjvC2ecDB4lBvwihAR6m+VTYv0q0tc3t/65XOnm2bY7+RHMhmck9ioIGbHKH7ld+TcnmftP1YgoSMDF4S3CxoH1VLSGGRYx2Rq7ESJq4AQtDl9R56CwNt9K8sbxzkM0YrK0qaEvFdb6HaDF4Ym+DgqrTVr+HNRWTyuGxjtfkcyrq2vSXRGrkjpcev48gNvUevBHtBV9nVXjGjHWzJ/ayTz32qiUTUC4gp8P7M6J1eda/cVyS8wkv13W3OR0UWlxxIv1zj8Pbe8P5d5NI6ZpSs//ssriiBfryh8xHXlLsUd1aFrXO2/pJCREiB1rVxkq986Scur0c4gfCFR68UBItFhbykZxjnve0H2ndmoKVPovxOqyXamizGu45GoKm5SjVxch7iRqrFpDf1v5Je2R09tgTTNz7RCrw/xpeaQxXnJ94Ir5V6UgfrGxW56YsRamU6uUG9rUbHGTNg4UN3WIVc7+afkGw5tBdnJuz3UJuchJiLpGirU4Dxu/67W0qRlNqPoKAcRLt4lV0jW9/2FmdIrfx9UDYuA9BO/tLjpOrEVl1StvmvN1qdr7gHinJO9UQ9wiVkEXdWiGNjUjknYdq6NLrIKCLNUnGoTETBiSrI7VkRNreVOx0ad6EjJREJJtHaujGT3W8vtwEFVcBsVMCJI2ujo2FbJmh4+1fLdIsmEREOqASDJ0ddzCTxUf1mflcY4Bu9gPjZl0JPgCOd+AsXB0WKXk+hJ4oEGo7dRzToZXBrs3wp2osYowoBSSteSKG2LLSO4KQhYxVvGYBFYkX1eImRRVm9eh9Xm8WOVzZlh5plrM5HWwtpEugDmLCSv6ATB5J9kPvEnAhNqiL0cFoRkpVufLNGaQ9gwqRDo7ua2yc/XqayNKrM/dDqzk8k5uBLmsHDN909d1xZjJzTWLDet/Tj54tvxpaB0oC3ls8YiOeaTPSuB1UFjbZ4K8fauMMgzFGvTYoqaZg2qpNzw/VKytoHkeh2KdBv2c1ENsD6pH/pIBsYYX48M+KCVZh4AFsvTG08SKduAjDX+h6+iojsXRJNZ92SR/a8wkEWnVsRrbxFpHxBT2yUeRSIg57/uMB7H2w7EGxkzln2wHDOLNZBPrIhxrKxDroBYvvfY0SBDrMhyri8abE+U3J8BAg0tr5EWstURM5d7/H6WzzLaUGmI+f27cbxOrxyBWjZnSSz2/MNPWR3qqu+6RJzCPHuuoClaB3psJcCfHCtb/FdzbxhCeWGuJmKQfPIdioLEf6/eCqnI8Z0CsjxIxlfVp1/4wD8e66yrObVjnxOoMSm39FpIBBra0fQ/WvY6nFrEGYk2TpLaY6RxOMHi0db+jpUGsgVhH1bDmkgHWrbATa+FYx4q+NQzruBrW3Y0Wz0/dhUTCxZbTJiPhMKyzalibkgHWY+GZ7KNLjRPeWJj71keKmHbeb2BriZGxprZiQp9ZpkeKmHYaYuudWMuqvTZV6o+Z6of9U2ib2tpUZN/I9GYmnZ+xMPdIEdPOSgpE7vAYtinrn3+jtCbWR4qYdqZWKMa74x3XoZ+RpWs1ZXcEvOKDS66SNb+DF9OR29N3YEPDFrXaIqYfHjQ19DqVP6+w9ayyHF2RbCitLWLaoVvjmtZ3opN8QiMDb31LrG6PGBoziQW+a9Dyr3z3cwdWnqbE6mIhf85sT5Yu0ycml8WoaeTeCw3QYr1kZ1bE6tKbrXkhdLxuWvhMjXAqqutPeknfMFL2yXFj3aDxiVvVuv6guvztx6XPB8/AxfFK6ZeLGuvAkhlyXfKgSo5vkBW5nnp7Zxx1wuI3PU+1To2osY4SvbFTve3c6xcLb6yQX/YyVXdc3at9M/5e7auKGuvCkKrTt4vOYtDJiwcejregrbwm+rvW/xjk5UQPrKLGukzsA7iXgqeH8Y/fP5z9+9PlxqGK2JbLP8jPuY+osdojJmHeG5VzG22v7w7qlosZ6wDN9vk3jPOqJfmtEijb+jSixzry7hHgNHIOWGGoZlsp8ZUTq6sGAp1HXDtvNtAKd2upPmTEWiFiEjIGzYplvraarjAUCIm17Au3yFXH7mDltyAiUzVlDMgdsTrN3SrII+/sX1qD+Qw8cDsnVp2PLX5eKSldkw0O0/lOj1hdEVMj0HZPKxnQXF83xj1S3Fhn/q0fHmk1exWCJqEkO6tuyCPGug6KmDwl1yB1bUElclMuImqsKdwMpqUP93VlXTHWCdvj5MQKZAEr5BxHVQOmEHU98K8tm7COQ1KH8p1Pe8HqmmNu3zpIvFhDiq1aydWsro50ZbqpOEi8WNdJ6NUz77y+Cs7yh+5dc2L1JHOm6JV9r/m2aFoTX3Q2jY8W6zAJfoqRP9jCzXDXHaQNN5U0Plqs5W1KbVsj2Aw/8/zIa3SQLz1ixZIK5vThzz+cgha0bfP9jmaXjFjBFKA5ZmqHWNCuf58M+uhVj1h9EdO2gqYXiwQXVaJg0+K47hGrN1WU4xfrJb2LYCC2QZ72iLWGYiu8Ji6qUwUGedojVpsdtVnwIyuSa+h33m3CqEaLdRmcOvxve/eOgjAMAGBYFHT2cQAn9+JjL3oDwV1EvIFe3+egaQqtHSzm+3ZT49/YLiHRt9RevSRlJ8kW80y/uzVSzTrtNHjdqrbWx6ey6Z2zylcqPwu9nw3/NOs6OPyxxq91Ce3rfPdR4ePRJ3N8J1Xlpfpa9dEF+7GFLnJLhafPVrlSOKfsR1lbb7IqNOke8rqjFAcZHPO2TjmFrDfL9y1ug9n2q0HmwSB5e+ebSNb7s2L5+B88bjeNBtk9B1m0e7LpZE2KrLIiK7IiK7LKKqusyIqsyIqsssoqK7IiK7Iiq6zIiqzIiqyyyiorsiIrsiKrrLLKiqzIiqzIKqussiIrsiIrssqKrMiKrMgqq6yyIiuyIiuyyiqrrMiKrMiKrLIiK7IiK7Iiq6zIiqzIiqyyyppKVgAAAAAAAAAAAAAAABq5Au3PMdRA2jE1AAAAAElFTkSuQmCC";
      const logoXPosition = 1;
      const logoWidth = 1.46;
      const logoAspectRatio = 0.3961;
      const logoHeight = logoWidth * logoAspectRatio;
      doc.addImage(logo, "", logoXPosition, logoYAxis, logoWidth, logoHeight);

      // header 1
      const mainHeader = ["Backup Code for Account Recovery"];
      this.docAddText(
        doc,
        this.header1,
        mainHeader,
        defaultMargin,
        mainHeaderYAxis,
        defaultLineHeight
      );

      // backup code section
      const backupCodeHeader = ["Save this somewhere safe"];
      this.docAddText(
        doc,
        this.header2,
        backupCodeHeader,
        defaultMargin,
        savedHeaderYAxis,
        defaultLineHeight
      );

      const backupSentence1 = [
        "This is your backup code for " +
          this.getDomain() +
          ". Use this to recover your account",
        "if you forget your password.",
      ];
      this.docAddText(
        doc,
        this.paragraph,
        backupSentence1,
        defaultMargin,
        backupSentence1YAxis,
        defaultLineHeight
      );

      const backupSentence2 = [
        "Either print this out and put it in a secure location, or save it on a USB or",
        "external hard drive.",
      ];
      this.docAddText(
        doc,
        this.paragraph,
        backupSentence2,
        defaultMargin,
        backupSentence2YAxis,
        defaultLineHeight
      );

      const backupSentence3 = [
        "Remember, anyone who gains access to this code and your email account",
        "can access your Passit account.",
      ];
      this.docAddText(
        doc,
        this.paragraph,
        backupSentence3,
        defaultMargin,
        backupSentence3YAxis,
        defaultLineHeight
      );

      // recover account section
      const recoveryCodeHeader = ["Recover your account"];
      this.docAddText(
        doc,
        this.header2,
        recoveryCodeHeader,
        defaultMargin,
        recoveryHeaderYAxis,
        defaultLineHeight
      );

      const recoveryCodeParagraph = [
        "1. Go to " + this.getDomain(),
        "2. On the login page, press 'Recover your account'",
        "3. Confirm your email address",
        "4. Scan the QR code below, or paste/type in the code manually",
      ];
      this.docAddText(
        doc,
        this.paragraph,
        recoveryCodeParagraph,
        defaultMargin,
        recoveryParagraphYAxis,
        defaultLineHeight
      );

      // rectangle
      doc.setLineWidth(0.02);
      doc.setDrawColor(229, 229, 229);
      doc.rect(1, rectangleYAxis, 6.5, 2.85);

      const paragraphIssues = [
        "If you have issues scanning the QR code, ",
        "enter the code below manually.",
      ];
      const paragraphIssuesMargin = 3.5;
      this.docAddText(
        doc,
        this.paragraph,
        paragraphIssues,
        paragraphIssuesMargin,
        issuesParagraphYAxis,
        defaultLineHeight
      );

      const dateTimeStamp = [this.getDate()];
      // const dateTimeStamp = ["Created on November 7, 2018 at 10:28 AM."];
      this.docAddText(
        doc,
        this.aside,
        dateTimeStamp,
        paragraphIssuesMargin,
        dateTimeStampYAxis,
        defaultLineHeight
      );

      // Add the qr code and text version of the code
      this.svgToPdf(doc, image, 1.2, qrCodeYAxis, 200, 200).then(() => {
        // Split up in groups of 4, spaces are fine because we'll strip them out on re-entry
        const parsedCode = [
          code
            .replace(/[^\dA-Z]/g, "")
            .replace(/(.{4})/g, "$1 ")
            .trim(),
        ];
        const codeMargin = 1.5;
        this.docAddText(
          doc,
          this.qrText,
          parsedCode,
          codeMargin,
          recoveryCodeYAxis,
          defaultLineHeight
        );

        doc.save("Passit Backup Code.pdf");
      });
    });
  }
}
