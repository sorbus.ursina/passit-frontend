import { createAction, props } from "@ngrx/store";
import { IAuthStore } from "../../user";
import { ISecret, IGroup } from "../../../../passit_sdk/api.interfaces";

export const init = createAction(
  "[Reset Password Verify] Init",
  props<{ email: string | null; code: string | null }>()
);
export const verifyAndLogin = createAction(
  "[Reset Password Verify] Verify and Login",
  props<{ payload: string }>()
);
export const verifyAndLoginSuccess = createAction(
  "[Reset Password Verify] Verify and Login Success",
  props<{ auth: IAuthStore; secrets: ISecret[]; groups: IGroup[] }>()
);

export const verifyAndLoginFailure = createAction(
  "[Reset Password Verify] Verify and Login Failure",
  props<{ payload: any }>()
);
export const setError = createAction(
  "[Reset Password Verify] Set error",
  props<{ payload: string }>()
);
