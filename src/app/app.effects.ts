import { Injectable } from "@angular/core";
import { Effect, ofType, Actions } from "@ngrx/effects";
import { tap, exhaustMap } from "rxjs/operators";
import { AppActionTypes } from "./app.actions";
import {
  AccountActionTypes,
  LogoutSuccessAction
} from "./account/account.actions";
import { Router } from "@angular/router";
import { UserService } from "./account/user";

@Injectable()
export class AppEffects {
  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$.pipe(
    ofType(AppActionTypes.LOGIN_REDIRECT, AccountActionTypes.LOGOUT_SUCCESS),
    tap(() => {
      this.router.navigate(["/account/login"]);
    })
  );

  @Effect()
  logout$ = this.actions$.pipe(
    ofType(AccountActionTypes.LOGOUT),
    exhaustMap(() =>
      this.userService
        .logout()
        .then(() => new LogoutSuccessAction())
        .catch(() => new LogoutSuccessAction())
    )
  );

  @Effect({ dispatch: false })
  logoutSuccess$ = this.actions$.pipe(
    ofType(AccountActionTypes.LOGOUT_SUCCESS),
    tap(() => localStorage.clear()),
    tap(() => this.router.navigate(["/account/login"]))
  );

  @Effect({ dispatch: false })
  userMustConfirmEmail$ = this.actions$.pipe(
    ofType(AccountActionTypes.USER_MUST_CONFIRM_EMAIL),
    tap(() => this.router.navigate(["/account/confirm-email"]))
  );

  constructor(
    private actions$: Actions,
    private userService: UserService,
    private router: Router
  ) {}
}
