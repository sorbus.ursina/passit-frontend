import { VerifyMFAEffects } from "./verify-mfa/verify-mfa.effects";
import { LoginEffects } from "./login.effects";
import { VerifyU2FEffects } from "./verify-mfa/verify-u2f/verify-u2f.effects";

export const effects = [LoginEffects, VerifyMFAEffects, VerifyU2FEffects];
