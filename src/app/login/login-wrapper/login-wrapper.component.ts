import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-login-wrapper",
  templateUrl: "./login-wrapper.component.html",
  styleUrls: [
    "../../account/account.component.scss",
    "./login-wrapper.component.scss"
  ]
})
export class LoginWrapperComponent {
  @Input() isPopup: boolean;
  @Input() linkText: string;
  @Input() linkRoute: string;
  @Output() clickLink = new EventEmitter();
}
