import { of as observableOf } from "rxjs";
import { TestBed, fakeAsync, tick } from "@angular/core/testing";
import { StoreModule, Store } from "@ngrx/store";

import * as fromRoot from "../app.reducers";
import * as fromLogin from "./login.reducer";
import { EffectsModule } from "@ngrx/effects";
import { SetValueAction } from "ngrx-forms";
import { UserService } from "../account/user";
import { NgPassitSDK } from "../ngsdk/sdk";
import { RouterTestingModule } from "@angular/router/testing";
import { LoginEffects } from "./login.effects";
import { pairwise } from "rxjs/operators";

describe("Login Effects", () => {
  let service: any;
  let effect: LoginEffects;
  let store: Store<fromRoot.IState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot(fromRoot.reducers),
        StoreModule.forFeature("login", fromLogin.reducers),
        EffectsModule.forRoot([LoginEffects]),
        RouterTestingModule
      ],
      providers: [
        { provide: NgPassitSDK },
        Store,
        {
          provide: UserService,
          useValue: jasmine.createSpyObj("UserService", ["checkUrl"])
        }
      ]
    });

    effect = TestBed.inject(LoginEffects);
    service = TestBed.inject(UserService);
    store = TestBed.inject(Store);
  });

  describe("asyncServerUrlCheck$", () => {
    it("should start async validation on any url value change", fakeAsync(() => {
      service.checkUrl.and.returnValue(observableOf(""));
      store.dispatch(new SetValueAction("Login Form.showUrl", true));
      store.dispatch(new SetValueAction("Login Form.url", "example.com"));
      effect.asyncServerUrlCheck$
        .pipe(pairwise())
        .subscribe(([action1, action2]) => {
          expect(action1.type).toBe("ngrx/forms/START_ASYNC_VALIDATION");
          expect(action2.type).toBe("ngrx/forms/CLEAR_ASYNC_ERROR");
        });
      tick(300);
    }));

    it("should not submit double service requests on rapid changes", fakeAsync(() => {
      service.checkUrl.and.returnValue(observableOf(""));
      let actionCount = 0;
      effect.asyncServerUrlCheck$.subscribe(result => {
        actionCount += 1;
      });
      store.dispatch(new SetValueAction("Login Form.showUrl", true));

      // First change
      store.dispatch(new SetValueAction("Login Form.url", "exam"));
      tick(10);
      // Quickly makes second change
      store.dispatch(new SetValueAction("Login Form.url", "example.com"));

      tick(600);
      // Two changes made quickly results in one async check
      expect(actionCount).toBe(2); // (two actions for one check)

      // Make another change (now that time has passed)
      store.dispatch(new SetValueAction("Login Form.url", "eexample.com"));
      expect(actionCount).toBe(2);
      tick(300);
      // Now we have 2 async checks
      expect(actionCount).toBe(4);
    }));
  });
});
