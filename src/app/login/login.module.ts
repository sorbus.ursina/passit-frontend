import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import * as fromLogin from "./login.reducer";
import { LoginComponent } from "./login.component";
import { LoginContainer } from "./login.container";
import { SharedModule } from "../shared/shared.module";
import { NgrxFormsModule } from "ngrx-forms";
import { ProgressIndicatorModule } from "../progress-indicator/progress-indicator.module";
import { VerifyMfaComponent } from "./verify-mfa/verify-mfa.component";
import { VerifyU2fComponent } from "./verify-mfa/verify-u2f/verify-u2f.component";

import { VerifyMfaContainer } from "./verify-mfa/verify-mfa.container";
import { LoginWrapperComponent } from "./login-wrapper/login-wrapper.component";
import { effects } from "./login.common";

export const COMPONENTS = [
  LoginComponent,
  LoginContainer,
  VerifyMfaComponent,
  VerifyU2fComponent,
  VerifyMfaContainer,
  LoginWrapperComponent
];

@NgModule({
  declarations: COMPONENTS,
  imports: [
    CommonModule,
    StoreModule.forFeature("login", fromLogin.reducers),
    EffectsModule.forFeature(effects),
    SharedModule,
    NgrxFormsModule,
    ProgressIndicatorModule
  ],
  exports: [LoginContainer, VerifyMfaContainer]
})
export class LoginModule {}
