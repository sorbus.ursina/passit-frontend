import { Action } from "@ngrx/store";

export enum VerifyU2fActionTypes {
  VERIFY_U2F = "[Verify U2F] Verify",
  VERIFY_U2F_SUCCESS = "[Verify U2F] Verify Success",
  VERIFY_U2F_FAILURE = "[Verify U2F] Verify Failure",
}

export class VerifyU2f implements Action {
  readonly type = VerifyU2fActionTypes.VERIFY_U2F;
}

export class VerifyU2fSuccess implements Action {
  readonly type = VerifyU2fActionTypes.VERIFY_U2F_SUCCESS;
}

export class VerifyU2fFailure implements Action {
  readonly type = VerifyU2fActionTypes.VERIFY_U2F_FAILURE;
}

export type VerifyU2fActions =
  | VerifyU2f
  | VerifyU2fSuccess
  | VerifyU2fFailure;
