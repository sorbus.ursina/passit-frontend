import { VerifyMfaActions, VerifyMfaActionTypes } from "../verify-mfa.actions";
import { VerifyU2fActions, VerifyU2fActionTypes } from "./verify-u2f.actions";

export interface IVerifyU2fState {
awaitingResponse: boolean;
errorMessage: string | null;
}

export const initialState: IVerifyU2fState = {
awaitingResponse: false,
errorMessage: null
};

export function reducer(
state = initialState,
action: VerifyU2fActions | VerifyMfaActions
): IVerifyU2fState {
state = { ...state};

switch (action.type) {
    case VerifyU2fActionTypes.VERIFY_U2F:
    return {
        ...state,
        awaitingResponse: true
    };

    case VerifyU2fActionTypes.VERIFY_U2F_SUCCESS:
    return {
        ...state,
        awaitingResponse: false
    };

    case VerifyU2fActionTypes.VERIFY_U2F_FAILURE:
    return {
        ...state,
        awaitingResponse: false,
        errorMessage: "Authentication was unsuccessful."
    };

    case VerifyMfaActionTypes.SWITCH_MFA_METHOD:
    return {
        ...state,
        errorMessage: null
    };
}
return state;
}
