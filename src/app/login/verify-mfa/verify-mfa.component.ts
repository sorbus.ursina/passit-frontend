import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { FormGroupState } from "ngrx-forms";
import { IVerifyMfaForm } from "./verify-mfa.reducer";

@Component({
  selector: "app-verify-mfa",
  templateUrl: "./verify-mfa.component.html",
  styleUrls: ["./verify-mfa.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VerifyMfaComponent {
  @Input() form: FormGroupState<IVerifyMfaForm>;
  @Input() errorMessage: string;
  @Input() isPopup: boolean;
  @Input() hasStarted: boolean;
  @Input() hasFinished: boolean;
  @Input() isExtension: boolean;
  @Input() hasU2F: boolean;
  @Input() useU2F: boolean;

  @Output() goToLogin = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  @Output() switchMfaMethod = new EventEmitter();
}
