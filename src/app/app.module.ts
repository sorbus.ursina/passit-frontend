import { APP_BASE_HREF } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule, Injectable } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Params } from "@angular/router";
import { ServiceWorkerModule } from "@angular/service-worker";
import { MicroSentryModule } from "@micro-sentry/angular";

import { EffectsModule } from "@ngrx/effects";
import {
  RouterStateSerializer,
  StoreRouterConnectingModule,
} from "@ngrx/router-store";
import { StoreModule } from "@ngrx/store";
import { InlineSVGModule } from "ng-inline-svg";
import { SelectModule } from "ng-select";
import { TooltipModule } from "ng2-tooltip-directive";
import { HotkeyModule } from "angular2-hotkeys";
import { AppComponent } from "./app.component";
import { ResetFormModule } from "./form/reset-form.module";
import { AlreadyLoggedInGuard, LoggedInGuard } from "./guards";
import { ListModule } from "./list";
import { NoContentComponent } from "./no-content/no-content.component";
import { NoContentContainer } from "./no-content/no-content.container";
import { SharedModule } from "./shared/shared.module";
import { ExtensionModule } from "./extension/extension.module";
import { ProgressIndicatorModule } from "./progress-indicator/progress-indicator.module";
import { metaReducers, reducers } from "./app.reducers";
import { IS_EXTENSION } from "./constants";
import { GetConfService } from "./get-conf/";
import { GetConfEffects } from "./get-conf/conf.effects";
import { Api } from "./ngsdk/api";
import { NgPassitSDK } from "./ngsdk/sdk";
import { GeneratorService } from "./secrets";
import { SecretEffects } from "./secrets/secret.effects";
import { SecretService } from "./secrets/secret.service";
import { AppDataService } from "./shared/app-data/app-data.service";
import { PopupLoggedInGuard } from "./guards/popup-logged-in.guard";
import { AuthInterceptor } from "./api/auth.interceptor";
import { devtoolsModule } from "./store-devtools";
import { UserService } from "./account/user";
import { AppEffects } from "./app.effects";
import { LoginModule } from "./login/login.module";
import { environment } from "../environments/environment";
import { routingStore, AppRoutingModule } from "./app-routing.module";
import { GroupService } from "./groups/group.service";
import { GroupEffects } from "./data/groups.effects";

// Why is this not default ngrx store, why is crashing default?
export interface IRouterStateUrl {
  url: string;
  queryParams: Params;
}

@Injectable()
export class CustomSerializer
  implements RouterStateSerializer<IRouterStateUrl>
{
  serialize(routerState: any): IRouterStateUrl {
    const { url } = routerState;
    const queryParams = routerState.root.queryParams;

    // Only return an object including the URL and query params
    // instead of the entire snapshot
    return { url, queryParams };
  }
}

/* tslint:disable:object-literal-sort-keys */
@NgModule({
  declarations: [AppComponent, NoContentContainer, NoContentComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HotkeyModule.forRoot(),
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreRouterConnectingModule.forRoot(),
    EffectsModule.forRoot([
      AppEffects,
      SecretEffects,
      GetConfEffects,
      GroupEffects,
    ]),
    devtoolsModule,
    FormsModule,
    ListModule,
    LoginModule,
    HttpClientModule,
    InlineSVGModule.forRoot(),
    MicroSentryModule.forRoot({}),
    ProgressIndicatorModule,
    ReactiveFormsModule,
    ResetFormModule,
    routingStore,
    SelectModule,
    !IS_EXTENSION
      ? ServiceWorkerModule.register("ngsw-worker.js", {
          enabled: environment.production,
        })
      : [],
    SharedModule,
    TooltipModule,
    ExtensionModule,
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: "/",
    },
    UserService,
    GroupService,
    GetConfService,
    { provide: AppDataService, useClass: AppDataService },
    AlreadyLoggedInGuard,
    LoggedInGuard,
    PopupLoggedInGuard,
    { provide: SecretService, useClass: SecretService },
    GeneratorService,
    { provide: RouterStateSerializer, useClass: CustomSerializer },
    { provide: Api, useClass: Api },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: NgPassitSDK, useClass: NgPassitSDK },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor() {}
}
