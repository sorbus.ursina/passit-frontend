import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Store, select } from "@ngrx/store";
import { getIsLoggedIn } from "../app.reducers";
import { getIsPrivateOrgMode, IState } from "./../app.reducers";

@Component({
  selector: "no-content-container",
  template: `
    <no-content
      [isLoggedIn]="isLoggedIn$ | async"
      [isPrivateOrgMode]="isPrivateOrgMode$ | async"
      [currentRoute]="currentRoute"
    ></no-content>
  `
})
export class NoContentContainer {
  currentRoute = "";
  isLoggedIn$ = this.store.pipe(select(getIsLoggedIn));
  isPrivateOrgMode$ = this.store.pipe(select(getIsPrivateOrgMode));

  constructor(private router: Router, private store: Store<IState>) {
    this.router.events.subscribe((route: any) => {
      this.currentRoute = route.url;
    });
  }
}
