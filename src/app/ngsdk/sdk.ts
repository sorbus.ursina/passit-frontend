import { Injectable } from "@angular/core";
import PassitSDK from "../../passit_sdk";
import { Api } from "./api";
import { HttpHeaders, HttpClient } from "@angular/common/http";

/**
 * Angular wrapper of PassitSDK that swaps out the api class with one that uses
 * $http instead of fetch. This is done for better integration with angular
 * tooling like Protractor.
 */
@Injectable()
export class NgPassitSDK extends PassitSDK {
  constructor(api: Api, private http: HttpClient) {
    super();
    this.api = api;
  }

  /** Get full URL, for example passing /api/my-thing/ might return https://passit.example.com  */
  formUrl(url: string) {
    return this.api.baseUrl + url;
  }

  post<T>(url: string, data: object) {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "token " + this.api.token
      })
    };
    return this.http.post<T>(url, data, httpOptions);
  }
}
