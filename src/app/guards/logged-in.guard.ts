import { map, take } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Store, select } from "@ngrx/store";

import { LoginRedirect } from "../app.actions";
import { IState, selectAuthState } from "../app.reducers";

@Injectable()
export class LoggedInGuard implements CanActivate {
  constructor(public store: Store<IState>, public router: Router) {}

  public canActivate() {
    return this.store.pipe(select(selectAuthState)).pipe(
      take(1),
      map(authState => {
        if (!authState.userToken) {
          this.store.dispatch(new LoginRedirect());
          return false;
        } else if (authState.forceSetPassword) {
          this.router.navigate(["/account/reset-password/set-password"]);
          return false;
        }

        return true;
      })
    );
  }
}
