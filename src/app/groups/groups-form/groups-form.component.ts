import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { FormGroupState } from "ngrx-forms";
import { IGroupFormValue } from "./groups-form.reducer";
import { IMultiselectList } from "../../shared/multiselect/multiselect-list/multiselect-list.component";
import { IGroupContact } from "../interfaces";

@Component({
  selector: "groups-form",
  templateUrl: "./groups-form.component.html",
  styleUrls: ["../../list/secret-row/secret-row.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupsFormComponent {
  @Input() form: FormGroupState<IGroupFormValue>;
  _contacts: IGroupContact[];
  listItems: IMultiselectList[];
  @Input() errorMessage: string;
  @Input() isNew: boolean;
  @Input() isUpdating: boolean;
  @Input() isUpdated: boolean;
  @Input() userId: number;
  @Input() isPrivateOrgMode: boolean;
  @Input() keyControls: boolean;
  @Input() searchFieldValidationPattern: RegExp;
  @Input() invalidSearchFieldText: string;
  @Input() noSearchFieldMatchText: string;
  @Input() noListItemsText: string;
  @Input() fewListItemsText: string;
  @Input() allListItemsSelectedText: string;
  @Input() justAddedText: string;
  @Output() toggleKeyControls = new EventEmitter();
  @Output() submitForm = new EventEmitter();
  @Output() delete = new EventEmitter();
  @Output() cancelForm = new EventEmitter();
  @Output() addUserToGroup = new EventEmitter<number>();
  @Output() removeUserFromGroup = new EventEmitter<number>();

  @Input("contacts") set contacts(value: IGroupContact[]) {
    this._contacts = value;
    this.listItems = this._contacts
      .map(contact => {
        return {
          name: contact.email,
          value: contact.id,
          isSelected: contact.isSelected,
          isPending: contact.isPending,
          confirmRemoval: contact.id === this.userId ? true : false // Confirm if removing yourself
        };
      })
      // Alpha order for email addresses https://stackoverflow.com/a/1129270
      .sort((a, b) => (a.name > b.name ? 1 : b.name > a.name ? -1 : 0))
      .filter(listItem => !this.isNew || listItem.value !== this.userId); // Don't show yourself in new group
  }

  get contacts() {
    return this._contacts;
  }

  onDelete() {
    if (window.confirm("Once it's deleted, it's gone forever. Is that okay?")) {
      this.delete.emit();
    }
  }
}
