import { createReducer, on } from "@ngrx/store";
import {
  onNgrxForms,
  updateGroup,
  validate,
  createFormGroupState,
  FormGroupState,
  wrapReducerWithFormStateUpdate
} from "ngrx-forms";
import { required } from "ngrx-forms/validation";
import { setManaged, showCreate, initGroups } from "../groups.actions";
import {
  addUserToGroup,
  removeUserFromGroup,
  saveGroup,
  saveGroupSuccess,
  saveGroupFailure
} from "./groups-form.actions";
import { IContact } from "../../data/interfaces";
import { IGroupContact } from "../interfaces";

const FORM_ID = "groupForm";

export interface IGroupFormValue {
  name: string;
  searchUsers: string;
}

export const initialGroupFormValue: IGroupFormValue = {
  name: "",
  searchUsers: ""
};

export const validateGroupForm = updateGroup<IGroupFormValue>({
  name: validate(required)
});

export const setFormValues = (values: IGroupFormValue) =>
  createFormGroupState<IGroupFormValue>(FORM_ID, values);

export interface IGroupMemberInfo {
  userId: number;
  isPending: boolean;
}

export interface IGroupFormState {
  form: FormGroupState<IGroupFormValue>;
  groupMembers: IGroupMemberInfo[];
  isUpdating: boolean;
  isUpdated: boolean;
  errorMessage: string | null;
}

const initialState: IGroupFormState = {
  form: createFormGroupState(FORM_ID, initialGroupFormValue),
  groupMembers: [],
  isUpdating: false,
  isUpdated: false,
  errorMessage: null
};

const rawReducer = createReducer(
  initialState,
  onNgrxForms(),
  on(setManaged, (state, action) => {
    return {
      ...state,
      form: setFormValues({ ...initialGroupFormValue, name: action.name }),
      groupMembers: action.groupuser_set.map(groupUser => ({
        userId: groupUser.user,
        isPending: groupUser.is_invite_pending
      })),
      isUpdated: false
    };
  }),
  on(initGroups, () => ({
    ...initialState
  })),
  on(showCreate, state => ({
    ...state,
    form: setFormValues({ ...initialGroupFormValue }),
    groupMembers: [],
    isUpdating: false,
    isUpdated: false,
    errorMessage: null
  })),
  on(saveGroup, state => ({ ...state, isUpdating: true, errorMessage: null })),
  on(saveGroupSuccess, state => ({
    ...state,
    isUpdating: false,
    isUpdated: true
  })),
  on(saveGroupFailure, state => ({ ...state, isUpdating: false })),
  on(addUserToGroup, (state, action) => ({
    ...state,
    form: setFormValues({ ...initialGroupFormValue, name: state.form.value.name }),
    groupMembers: [...state.groupMembers].concat({
      userId: action.userId,
      isPending: !action.isPrivateOrgMode,
    })
  })),
  on(removeUserFromGroup, (state, action) => ({
    ...state,
    groupMembers: [...state.groupMembers].filter(
      member => member.userId !== action.userId
    )
  }))
);

export const reducer = wrapReducerWithFormStateUpdate(
  rawReducer,
  s => s.form,
  validateGroupForm
);

export const selectForm = (state: IGroupFormState) => state.form;
export const selectGroupMembers = (state: IGroupFormState) => {
  return state.groupMembers;
};
export const selectGroupContacts = (
  contacts: IContact[],
  groupMembers: IGroupMemberInfo[]
): IGroupContact[] =>
  contacts.map(contact => {
    const groupMember = groupMembers.find(member => member.userId === contact.id);
    return {
      ...contact,
      isPending: groupMember ? groupMember.isPending : false,
      isSelected: !!groupMember
    };
  });
export const selectIsUpdating = (state: IGroupFormState) => state.isUpdating;
export const selectIsUpdated = (state: IGroupFormState) => state.isUpdated;
