import { Component, Input } from "@angular/core";

@Component({
  selector: "group-add",
  styleUrls: [
    "../../list/secret-row/secret-row.component.scss",
    "../../list/list.component.scss"
  ],
  templateUrl: "./group-add.component.html"
})
export class GroupAddComponent {
  @Input() showCreate: boolean;
}
