import {
  ActionReducer,
  createSelector,
  MetaReducer,
  ActionReducerMap,
} from "@ngrx/store";
import { localStorageSync } from "ngrx-store-localstorage";
import { routerReducer, RouterReducerState } from "@ngrx/router-store";

import { AccountActionTypes } from "./account/account.actions";
import * as popupState from "./extension/popup/popup.reducer";
import * as confState from "./get-conf/conf.reducer";
import * as secretState from "./secrets/secrets.reducer";
import * as fromAuth from "./auth.reducer";
import * as fromGroup from "./data/groups.reducer";
import * as fromContacts from "./data/contacts.reducer";

export interface IState {
  auth: fromAuth.IAuthState;
  router: RouterReducerState;
  secrets: secretState.ISecretState;
  conf: confState.IConfState;
  groups: fromGroup.IGroupState;
  contacts: fromContacts.IContactState;
}

/** This is only used in testing */
export const initialState: IState = {
  auth: fromAuth.initialState,
  router: {} as any,
  secrets: secretState.initialState,
  conf: confState.initial,
  groups: fromGroup.initialState,
  contacts: fromContacts.initialState,
};

export const reducers: ActionReducerMap<IState> = {
  auth: fromAuth.authReducer,
  router: routerReducer,
  secrets: secretState.secretReducer,
  conf: confState.reducer,
  groups: fromGroup.reducer,
  contacts: fromContacts.reducer,
};

export function popupDeserialize(popup: popupState.IPopupState) {
  // Only rehydrate if popup was recently interacted with
  const now = new Date();
  const lastOpened = new Date(popup.lastOpened!);
  const FIVE_MIN = 5 * 60 * 1000;
  return now.getTime() - lastOpened.getTime() < FIVE_MIN
    ? popup
    : popupState.initialState;
}
const shouldSync = (state: IState) => state.auth.rememberMe;
export function localStorageSyncReducer(
  reducer: ActionReducer<any>
): ActionReducer<any> {
  // Type issue with ngrx-store-localstorage?
  const list: any = {
    list: ["searchText", "secretManaged", "firstTimeLoadingComplete"],
  };
  return localStorageSync({
    keys: [
      "auth",
      "contacts",
      "groups",
      {
        list: list,
      },
      "secrets",
      { popup: { deserialize: popupDeserialize } },
      "conf",
    ],
    rehydrate: true,
    syncCondition: shouldSync,
  })(reducer);
}

export function logout(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state: IState, action: any) =>
    reducer(
      action.type === AccountActionTypes.LOGOUT_SUCCESS ? undefined : state,
      action
    );
}

export const metaReducers: Array<MetaReducer<any>> = [
  localStorageSyncReducer,
  logout,
];

export const getSecretState = (state: IState) => state.secrets;
export const getSecrets = createSelector(
  getSecretState,
  secretState.getSecrets
);
export const getSecretsCount = createSelector(
  getSecrets,
  (secrets) => secrets.length
);

export const getConfState = (state: IState) => state.conf;
export const getIsPrivateOrgMode = createSelector(
  getConfState,
  confState.getIsPrivateOrgMode
);
export const getBillingEnabled = createSelector(
  getConfState,
  confState.getBillingEnabled
);
export const getIsPopup = createSelector(getConfState, confState.getIsPopup);
export const getTimestamp = createSelector(
  getConfState,
  confState.getTimestamp
);
export const getRavenDsn = createSelector(getConfState, confState.getRavenDsn);

export const getRouterState = (state: IState) => state.router;
export const getRouterPath = createSelector(
  getRouterState,
  (state: RouterReducerState) => {
    if (state) {
      return state.state.url;
    }
  }
);

export const selectAuthState = (state: IState) => state.auth;
export const getIsLoggedIn = createSelector(
  selectAuthState,
  (state: fromAuth.IAuthState) => (state.userToken ? true : false)
);
export const getToken = createSelector(
  selectAuthState,
  (state: fromAuth.IAuthState) => state.userToken
);
export const getEmail = createSelector(
  selectAuthState,
  (state: fromAuth.IAuthState) => state.email
);
export const getForceUserSetPassword = createSelector(
  selectAuthState,
  (state: fromAuth.IAuthState) => state.forceSetPassword && state.userToken // Sanity check user is logged in
);
export const getUserId = createSelector(
  selectAuthState,
  (state: fromAuth.IAuthState) => state.userId
);
export const getUrl = createSelector(
  selectAuthState,
  (state: fromAuth.IAuthState) => state.url
);
export const getOptInErrorReporting = createSelector(
  selectAuthState,
  (state: fromAuth.IAuthState) => state.optInErrorReporting
);
export const getMfaRequired = createSelector(
  selectAuthState,
  (state: fromAuth.IAuthState) => state.mfaRequired
);
export const hasU2F = createSelector(
  selectAuthState,
  (state: fromAuth.IAuthState) => {
    if (state.validAuth.includes("FIDO2")) {
      return true;
    } else {
      return false;
  }}
);

export const selectGroupsState = (state: IState) => state.groups;
export const selectAllGroups = createSelector(
  selectGroupsState,
  fromGroup.selectAllGroups
);

export const selectContactsState = (state: IState) => state.contacts;
export const selectAllContacts = createSelector(
  selectContactsState,
  fromContacts.selectAllContacts
);
