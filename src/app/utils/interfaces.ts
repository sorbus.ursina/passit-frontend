export interface IBaseFormState {
  hasStarted: boolean;
  hasFinished: boolean;
}
