import { ExporterComponent } from "./exporter.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ExporterService } from "./exporter.service";
import { FormsModule } from "@angular/forms";
import { ExporterRoutingModule } from "./exporter-routing.module";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  imports: [CommonModule, ExporterRoutingModule, FormsModule, SharedModule],
  declarations: [ExporterComponent],
  providers: [ExporterService]
})
export class ExporterModule {}
