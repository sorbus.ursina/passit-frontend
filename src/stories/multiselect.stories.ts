import { NgrxFormsModule } from "ngrx-forms";
import { InlineSVGModule } from "ng-inline-svg";
import { StoreModule } from "@ngrx/store";
import { HttpClientModule } from "@angular/common/http";
import { storiesOf, moduleMetadata } from "@storybook/angular";
import { withKnobs, boolean, number, select } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { RouterTestingModule } from "@angular/router/testing";

import { TextLinkComponent } from "../app/shared/text-link/text-link.component";
import { AsideLinkComponent } from "../app/shared/aside-link/aside-link.component";
import { TextFieldComponent } from "../app/shared/text-field/text-field.component";
import { HeadingComponent } from "../app/shared/heading/heading.component";
import { MultiselectComponent } from "../app/shared/multiselect/multiselect.component";
import { MultiselectListComponent } from "../app/shared/multiselect/multiselect-list/multiselect-list.component";
import { MultiselectAddComponent } from "../app/shared/multiselect/multiselect-add/multiselect-add.component";
import { BadgeComponent } from "../app/shared/multiselect/badge/badge.component";
import { FormLabelComponent } from "../app/shared/form-label/form-label.component";
import { CheckboxComponent } from "../app/shared/checkbox/checkbox.component";

import {
  groupMembers,
  secretGroups
} from "../app/shared/multiselect/sample-data";
import { StorybookModule } from "./storybook.module";
import { multiselectSecretText } from "../app/list/secret-form/secret-form.container";

storiesOf("Multiselect", module)
  .addDecorator(withKnobs)
  .addDecorator(
    moduleMetadata({
      imports: [
        RouterTestingModule,
        NgrxFormsModule,
        HttpClientModule,
        InlineSVGModule.forRoot(),
        StoreModule.forRoot({}),
        StorybookModule
      ],
      declarations: [
        AsideLinkComponent,
        FormLabelComponent,
        HeadingComponent,
        TextFieldComponent,
        TextLinkComponent,
        MultiselectComponent,
        MultiselectListComponent,
        MultiselectAddComponent,
        BadgeComponent,
        CheckboxComponent
      ]
    })
  )
  .add("Members for Group Form: Multiselect Complete", () => {
    const totalMemberCount = number("Total Members", 20, {
      range: true,
      min: 0,
      max: 50,
      step: 1
    });

    const selectedCount = number("Selected Members", 8, {
      range: true,
      min: 0,
      max: 50,
      step: 1
    });

    const makeArrayWithKnobSelectedCount = () => {
      // Ideally I'd have the "selected" knob dependent on the total knob but whatever
      const _totalMemberCount =
        totalMemberCount < selectedCount ? selectedCount : totalMemberCount;

      // Why have selected items ready only to delete them? Dunno, thought I might benefit from that elsewhere
      const noneSelected = groupMembers.slice(0, _totalMemberCount);
      noneSelected.forEach(item => {
        if (item.isSelected) {
          delete item.isSelected;
        }
        if (item.isPending) {
          delete item.isPending;
        }
      });

      // https://stackoverflow.com/a/2380113/
      const selectedIndices = [];
      while (selectedIndices.length < selectedCount) {
        const r = Math.floor(Math.random() * _totalMemberCount);
        if (selectedIndices.indexOf(r) === -1) {
          selectedIndices.push(r);
        }
      }

      selectedIndices.forEach(index => {
        noneSelected[index].isSelected = true;
        if (Math.random() > 0.5) {
          noneSelected[index].isPending = true;
        }
      });

      return noneSelected;
    };

    const listItems = makeArrayWithKnobSelectedCount();

    return {
      component: MultiselectComponent,
      props: {
        listItems,
        label: "Members",
        searchControl: {
          id: "id",
          value: "",
          errors: {}
        },
        privateOrgMode: boolean("Private Org Mode", false),
        singleItemIsRemovable: boolean("Single Item is removable", false),
        removeListItem: action("Remove from list")
      }
    };
  })
  .add("Members for Group Form: Multiselect List", () => {
    const listItemCount = number("List Item Count", 13, {
      range: true,
      min: 0,
      max: 50,
      step: 1
    });

    return {
      component: MultiselectListComponent,
      props: {
        listItems: groupMembers.slice(0, listItemCount),
        singleItemIsRemovable: boolean("Single Item is removable", true),
        removeListItem: action("Remove from list")
      }
    };
  })
  .add("Members for Group Form: Multiselect Add", () => {
    const listItemCount = number("List Item Count", 4, {
      range: true,
      min: 0,
      max: 50,
      step: 1
    });

    const privateOrgMode = boolean("Private Org Mode", false);

    type FeedbackState =
      | "no-list-items"
      | "few-list-items"
      | "just-added"
      | "all-selected"
      | "invalid-search"
      | "no-match-search"
      | "match-search"
      | "normal";
    const defaultFeedbackState: FeedbackState = "normal";
    const feedbackStateOptions: { [k: string]: FeedbackState } = {
      Normal: defaultFeedbackState,
      "No List Items": "no-list-items",
      "0-4 List Items": "few-list-items",
      "Just Added Item": "just-added",
      "All List Items Selected": "all-selected",
      "Invalid search field": "invalid-search",
      "No match to search field": "no-match-search",
      "Search field matches item in list": "match-search"
    };
    const feedbackStateControl: FeedbackState = select(
      "Feedback States",
      feedbackStateOptions,
      defaultFeedbackState
    );

    const getSearchValue = () => {
      if (feedbackStateControl === "invalid-search") {
        return "foo@";
      }
      if (feedbackStateControl === "no-match-search") {
        return "foo@bar.com";
      }
      if (feedbackStateControl === "match-search") {
        return "bahwi@opt";
      }
      return "";
    };

    // Should be AbstractControlState<string> but doesn't need to be
    const searchValue = {
      id: "id",
      value: getSearchValue(),
      errors: {}
    };

    const getListItems = () => {
      const members = [...groupMembers];
      if (privateOrgMode) {
        members.forEach(member => {
          member.isPending = false;
        });
      }
      if (feedbackStateControl === "no-list-items") {
        return [];
      }
      if (feedbackStateControl === "few-list-items") {
        return members.slice(0, 3);
      }
      if (feedbackStateControl === "all-selected") {
        return members.filter(member => member.isSelected === true);
      }
      if (feedbackStateControl === "match-search") {
        return members.slice(0, listItemCount < 4 ? 4 : listItemCount);
      }
      return members.slice(0, listItemCount);
    };

    return {
      props: {
        feedbackStateControl,
        ngrxFormControl: searchValue,
        modalMode: boolean("Modal Mode", false),
        label: "Members",
        listItems: getListItems(),
        totalListItemsCount: getListItems().length,
        keyControls: true,
        searchFieldValidationPattern: /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/,
        invalidSearchFieldText: "Enter a valid email address.",
        noSearchFieldMatchText: "No Passit user with this email address.",
        noListItemsText: privateOrgMode
          ? "Looks like you're all alone! You can still create groups as a way to organize your passwords."
          : "Enter a Passit user's complete email address above to invite that person to a group.",
        fewListItemsText: privateOrgMode
          ? "This list shows other Passit users that you can add to your groups."
          : "This list shows Passit users that you have added to your other group.",
        allListItemsSelectedText: privateOrgMode
          ? "Everyone is in the group!"
          : "",
        justAddedText: privateOrgMode
          ? "Once you press “Save Group”, ||| will be invited."
          : `
            Once you press “Save Group”, ||| will be invited. When people you invite
            accept your invitation, their email addresses will be visible here when you
            create or edit another group.
          `,
        addListItem: action("Add List Item"),
        removeListItem: action("Remove List Item"),
        toggleKeyControls: action("Toggle Key Controls"),
        closeModalMode: action("Close modalMode")
      },
      template: `
        <div style="padding: 20px;">
          <div style="margin-bottom: 20px; border-bottom: 1px solid black">
            <p>
              This component is doing a lot, and it can all be a bit hard to show in Storybook.
              If something requires special instructions, they'll appear here.
            </p>
            <p *ngIf="feedbackStateControl === 'just-added'">
              Trigger this feedback state by typing in an exact match to one of the items on the
              list, then press enter. Or, click on a list item. It'll show the messaging, but
              won't clear the search field or reset the visible list items.
            </p>
          </div>
          <app-multiselect-add
            [ngrxFormControl]="ngrxFormControl"
            [label]="label"
            [listItems]="listItems"
            [modalMode]="modalMode"
            [totalListItemsCount]="totalListItemsCount"
            [keyControls]="keyControls"
            [searchFieldValidationPattern]="searchFieldValidationPattern"
            [invalidSearchFieldText]="invalidSearchFieldText"
            [noSearchFieldMatchText]="noSearchFieldMatchText"
            [noListItemsText]="noListItemsText"
            [fewListItemsText]="fewListItemsText"
            [allListItemsSelectedText]="allListItemsSelectedText"
            [justAddedText]="justAddedText"
            (toggleKeyControls)="toggleKeyControls"
            (closeModalMode)="closeModalMode"
            (addListItem)="addListItem"
            (removeListItem)="removeListItem"
          ></app-multiselect-add>
        </div>
      `
    };
  })
  .add("Groups for Secret Form: Multiselect Complete", () => {
    const totalGroupCount = number("Total Groups", 5, {
      range: true,
      min: 0,
      max: secretGroups.length,
      step: 1
    });

    const selectedCount = number("Selected Groups", 2, {
      range: true,
      min: 0,
      max: secretGroups.length,
      step: 1
    });

    const makeArrayWithKnobSelectedCount = () => {
      const _totalGroupCount =
        totalGroupCount < selectedCount ? selectedCount : totalGroupCount;

      const noneSelected = secretGroups.slice(0, _totalGroupCount);
      noneSelected.forEach(item => {
        if (item.isSelected) {
          delete item.isSelected;
        }
      });

      // https://stackoverflow.com/a/2380113/
      const selectedIndices = [];
      while (selectedIndices.length < selectedCount) {
        const r = Math.floor(Math.random() * _totalGroupCount);
        if (selectedIndices.indexOf(r) === -1) {
          selectedIndices.push(r);
        }
      }

      selectedIndices.forEach(index => {
        noneSelected[index].isSelected = true;
      });

      return noneSelected;
    };

    const listItems = makeArrayWithKnobSelectedCount();

    return {
      component: MultiselectComponent,
      props: {
        listItems,
        label: "Groups",
        searchControl: {
          id: "id",
          value: "",
          errors: {}
        },
        privateOrgMode: boolean("Private Org Mode", false),
        singleItemIsRemovable: true,
        removeListItem: action("Remove from list")
      }
    };
  })
  .add("Groups for Secret Form: Multiselect Add", () => {
    const listItemCount = number("List Item Count", 4, {
      range: true,
      min: 0,
      max: 50,
      step: 1
    });

    type FeedbackState =
      | "no-list-items"
      | "few-list-items"
      | "just-added"
      | "all-selected"
      | "no-match-search"
      | "match-search"
      | "normal";
    const defaultFeedbackState: FeedbackState = "normal";
    const feedbackStateOptions: { [k: string]: FeedbackState } = {
      Normal: defaultFeedbackState,
      "No List Items": "no-list-items",
      "0-4 List Items": "few-list-items",
      "Just Added Item": "just-added",
      "All List Items Selected": "all-selected",
      "No match to search field": "no-match-search",
      "Search field matches item in list": "match-search"
    };
    const feedbackStateControl: FeedbackState = select(
      "Feedback States",
      feedbackStateOptions,
      defaultFeedbackState
    );

    const getSearchValue = () => {
      if (feedbackStateControl === "no-match-search") {
        return "Blarg";
      }
      if (feedbackStateControl === "match-search") {
        return "School";
      }
      return "";
    };

    // Should be AbstractControlState<string> but doesn't need to be
    const searchValue = {
      id: "id",
      value: getSearchValue(),
      errors: {}
    };

    const getListItems = () => {
      const groups = [...secretGroups];
      if (feedbackStateControl === "no-list-items") {
        return [];
      }
      if (feedbackStateControl === "few-list-items") {
        return groups.slice(0, 3);
      }
      if (feedbackStateControl === "all-selected") {
        return groups.filter(group => group.isSelected === true);
      }
      if (feedbackStateControl === "match-search") {
        return groups.slice(0, listItemCount < 4 ? 4 : listItemCount);
      }
      return groups.slice(0, listItemCount);
    };

    return {
      props: {
        feedbackStateControl,
        ngrxFormControl: searchValue,
        modalMode: boolean("Modal Mode", false),
        label: "Groups",
        listItems: getListItems(),
        totalListItemsCount: getListItems().length,
        keyControls: true,
        noSearchFieldMatchText: multiselectSecretText.noSearchFieldMatchText,
        noListItemsText: multiselectSecretText.noListItemsText,
        fewListItemsText: multiselectSecretText.fewListItemsText,
        allListItemsSelectedText: multiselectSecretText.allListItemsSelectedText,
        justAddedText: multiselectSecretText.justAddedText,
        addListItem: action("Add List Item"),
        removeListItem: action("Remove List Item"),
        toggleKeyControls: action("Toggle Key Controls"),
        closeModalMode: action("Close modalMode")
      },
      template: `
        <div style="padding: 20px;">
          <div style="margin-bottom: 20px; border-bottom: 1px solid black">
            <p>
              This component is doing a lot, and it can all be a bit hard to show in Storybook.
              If something requires special instructions, they'll appear here.
            </p>
            <p *ngIf="feedbackStateControl === 'just-added'">
              Trigger this feedback state by typing in an exact match to one of the items on the
              list, then press enter. Or, click on a list item. It'll show the messaging, but
              won't clear the search field or reset the visible list items.
            </p>
          </div>
          <app-multiselect-add
            [ngrxFormControl]="ngrxFormControl"
            [label]="label"
            [listItems]="listItems"
            [modalMode]="modalMode"
            [totalListItemsCount]="totalListItemsCount"
            [keyControls]="keyControls"
            [noSearchFieldMatchText]="noSearchFieldMatchText"
            [noListItemsText]="noListItemsText"
            [fewListItemsText]="fewListItemsText"
            [allListItemsSelectedText]="allListItemsSelectedText"
            [justAddedText]="justAddedText"
            (toggleKeyControls)="toggleKeyControls"
            (closeModalMode)="closeModalMode"
            (addListItem)="addListItem"
            (removeListItem)="removeListItem"
          ></app-multiselect-add>
        </div>
      `
    };
  });
