import { InlineSVGModule } from "ng-inline-svg";
import { HttpClientModule } from "@angular/common/http";
import { storiesOf, moduleMetadata } from "@storybook/angular";
import { action } from "@storybook/addon-actions";
import { boolean, select, number, withKnobs } from "@storybook/addon-knobs";
import { StoreModule } from "@ngrx/store";
import { NgrxFormsModule } from "ngrx-forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { BackupCodeComponent } from "../app/account/backup-code/backup-code.component";
import { ResetPasswordComponent } from "../app/account/reset-password/reset-password.component";
import { SharedModule } from "../app/shared/shared.module";
import { ProgressIndicatorModule } from "../app/progress-indicator/progress-indicator.module";
import * as fromResetPassword from "../app/account/reset-password/reset-password.reducer";
import * as fromChangePassword from "../app/account/change-password/change-password.reducer";
import * as fromDelete from "../app/account/delete/delete.reducer";
import * as fromSetPassword from "../app/account/reset-password/set-password/set-password.reducer";
import * as fromResetPasswordVerify from "../app/account/reset-password/reset-password-verify/reset-password-verify.reducer";
import { RouterTestingModule } from "@angular/router/testing";
import { ChangePasswordComponent } from "../app/account/change-password";
import { PasswordInputComponent } from "../app/account/change-password/password-input/password-input.component";
import { ManageBackupCodeComponent } from "../app/account/manage-backup-code/manage-backup-code.component";
import { BackupCodePdfService } from "../app/account/backup-code-pdf.service";
import { SetPasswordComponent } from "../app/account/reset-password/set-password/set-password.component";
import { ResetPasswordVerifyComponent } from "../app/account/reset-password/reset-password-verify/reset-password-verify.component";
import { DownloadBackupCodeComponent } from "../app/account/manage-backup-code/download-backup-code/download-backup-code.component";
import { DeleteComponent } from "~/app/account/delete/delete.component";
import { ManageMfaComponent } from "../app/account/manage-mfa/manage-mfa.component";
import { AccountComponent } from "../app/account/account.component";
import {
  ConfirmEmailContainer,
  ConfirmEmailComponent
} from "../app/account/confirm-email";
import { SplitMfaLinkPipe } from "../app/account/manage-mfa/split-mfa-link.pipe";

storiesOf("Account", module)
  .addDecorator(withKnobs)
  .addDecorator(
    moduleMetadata({
      imports: [
        InlineSVGModule.forRoot(),
        HttpClientModule,
        NgrxFormsModule,
        SharedModule,
        RouterTestingModule,
        ProgressIndicatorModule,
        BrowserAnimationsModule,
        StoreModule.forRoot({})
      ],
      declarations: [
        PasswordInputComponent,
        DownloadBackupCodeComponent,
        BackupCodeComponent,
        ConfirmEmailContainer,
        ConfirmEmailComponent,
        SplitMfaLinkPipe
      ]
    })
  )
  .add("Reset Password", () => ({
    component: ResetPasswordComponent,
    props: {
      isExtension: boolean("isExtension", true),
      hasStarted: boolean("hasStarted", false),
      hasFinished: boolean("hasFinished", false),
      form: fromResetPassword.initialFormState
    }
  }))
  .add("Reset Password Verify", () => {
    const isSubmitted = boolean("Is submitted", false);
    const isValid = boolean("Is valid", false);
    const form = {
      ...fromResetPasswordVerify.initialResetPasswordVerifyFormState,
      isSubmitted,
      isValid
    };

    return {
      component: ResetPasswordVerifyComponent,
      props: {
        form,
        verify: action("code entered"),
        showScanner: boolean("show scanner", false),
        hasStarted: boolean("has started", false),
        badCodeError: boolean("bad code error", false)
          ? "Passit does not recognize this backup code."
          : ""
      }
    };
  })
  .add("Set Password", () => ({
    component: SetPasswordComponent,
    props: {
      form: fromSetPassword.initialState.form,
      hasStarted: boolean("has started", false)
    }
  }))
  .add("Account Home", () => ({
    component: AccountComponent,
    props: {}
  }))
  .add("Change Password", () => ({
    component: ChangePasswordComponent,
    props: {
      form: fromChangePassword.initialState.form,
      hasStarted: boolean("hasStarted", true),
      hasFinished: boolean("hasFinished", true)
    }
  }))
  .add("Multi Factor Auth", () => ({
    component: ManageMfaComponent,
    props: {
      stepDownloadApp: number("step", 0),
      mfaEnabled: boolean("Two Factor Enabled", false),
      uri:
        "otpauth://totp/Passit:test%40example.com?secret=JE4ZRQPUIWJV6APW&issuer=Passit",
      qrCode:
        // tslint:disable-next-line:max-line-length
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAANIElEQVR4Xu2d0W7kOAwEk///6CxwT2cP4EKhKctxel9pUWSzSGs8zuz3z8/Pz1f/VYGXKPBdoF9SyabxnwIFuiC8SoEC/apyNpkCXQZepUCBflU5m0yBLgOvUqBAv6qcTaZAl4FXKVCgX1XOJlOgy8CrFCjQrypnkynQZeBVChToV5WzyRToMvAqBQr0q8rZZGKgv7+/b1Xx/Pp2uj/5S+2pOPS6+nT+53hT/zZ/ypf8FejT3zecC1ig9w4sAvijAdMX/Hd3cLp/CiyttwU5X08Tazr/TugeOQ4MpIAV6OwvAsePHDRR7MRKjwDTE8fmlwI+fQdI47frqd5UX1q//Mhxd8JWkGnASPDp/ab92fjvri/FV6DDI5It6DSA0/4IGDswyB/dMa2+BbpAK+YKNDwGIzXpzGjttJ+deHZ/CwRdvzpeO0HvjgfrOf3YjgpOAdF6a6f9VheEAH06QBT/av2ofrcfOVYnTIKTIE+Lj/JZHe/TGwzr2QntvgmzdwgC9OkAUfx3N9jrgbZAkCDkz66fbgALmN2f8id/pA+tf/xTjrs7mApOglNB7XpbQIp/tZ3yp3xIH1pfoE8KUsFJcCqoXW8LSPGvtlP+lA/pQ+sLdIE+KJACX6BPX1RQB053MBWQ9qP1ZCcAaH+rl51gq498q/2Tfn1sJyd6gT4+BbINeAaO1tuGLdAF+nLoWeA6oeEekgpqO5wmMNl75Fj7HP9xRw4bEAFigaX97YQhf2SnhqX19hZO19N+tqHJ3+r6/voX/K2AJGjqj9YX6KNC0w1ToMPXSQlgmpDpHcI2iL3DTQNHA8XGd/uHQlvw1QmT/zReWm8BnPZngfnzQFMBUjsBUfvxj0wJSGtP60frbcMtn9AUcGovsGuBJX3T+tH6Ai2/qaSC1X7dMARkai/QBfrA0OqGTIGl9duBpgB32+kpghXQ+kuvn46P6mH3I3932+PHdncHbPezQJF/6y+93gJG+1F+dj/yd7e9QJ/+Sp0KQMDQLf/sn663gFF8lJ/dj/zdbS/QBfryDH43kOl+MdD0HPPjOeHib+ZowlC8NOHsRLX7Wf8xAFCP1XrSHcvmV6DlD+NY4Ap09jZegYYjhAWMJoj1N90QtuD2jtkJLYFKC0LAUQEJMPJfoNf+RYvlY/zIYQMgoGhCELBpPHY9ncHJH+VL/klP26D2+nR/0ofsBfqkEAGFgoYfemn/An1dgQJdoA8KpA3TCS3/F6rdE3L3/ilw9ghhr3890JRgeoslwKyd4iF/9kMi+SOgaD3FQ+tXf0YZj2/610epADYBmki2IHR9gYYz6vBnBMsD1W/5GboTevb9Yttw48AUaPcXFtO3OOpouqPY9QRQescp0Dc/5bCCE8A04VMgUwDT+NL9KX9qIFuv6Xgpfj1Qps/QVqACvfabtgItW4I6Vrr7In9PK1An9N7PCB8DsRM6m5AF+uVA00S2R5J0ItN6inf8jCffP7Z3LNJ3tR4Urz1i2vosf2yXAkEF2F1ALXiBPkhmG4D0LtCk0MlODUTubINSwclOE5HiJTsduUgvGz/FU6BJoQJ9qdDrgH7aBJiegNP5pROLJlpqp/62+pJ+pAfFM/6UgwK2AdGZ2xYs9TedHxXQ5mcnJPmnehVoUgjsacEKdPbqgW3otGFDXL7iM7RN2AZcoB2QNIHJTvV5/YSmBEmg6QmaNhg1UBpvqpeNL9U/1XN6f/IXT+i0QCkgmKB83dECQ9eTneK3+lh/5L9Ah4oSAHRGSwtA+xMAdEtPB4CNz5aD9E3jp3hof1rfCX1SyAJD15OdCmQbyPoj/+mAsPFsB5oCTjvaAhELMvwD6gQMTXTSl+zkn+zkPwU+rdfH/unbdpRwgT4qRA06XuCwQam+BVoqRADQBJTb6fex0/imJyQBRvGmDWUHWLpfJzQQToBZOzUc+bMNWaDDFpnuSOsvBSYFiuK18lI81k4NYeMjf6QHrU/juf0pBwU8LYgFgOKjCUgNRgVN47X62Xxt/HR9qtf4kWNaQOuPBEkBoYJQvBaYNF6Kh/SifMlu95+OpxMangKkBSzQpOD1UyC3+mv+5SQdQAiU3c9ebyem9W+BpyMP+aMJatfTU5PpCUz6xhOaNiA7AUPrV9spPgKE4iOAaD3FRw1ggaP9SI80X9Rj9RcrGEAnNEl0aSfACnQkr19sC+J3yFZQfDSRaPd0YlF8BZoqsNieAkLh0ZmPACOAyE7xpesJ4DR/in/3/tvP0CSAFZCuTwtKwJGd4kvXk55p/hT/7v0LtDzDE3BkJyDS9buB2r1/gS7QBwbpyEUN+eeBnp5IJOjZTrdguj4tMPknfegzBwFq19P1aT5Wz496v/2xHRWgQF//X9xWnwItb/m2gwv08WcQVt/BCnSBvjzD9sjhRlj8oZAmIHVsOjHS9TZ+yof80Zk2zSddb+OnhqN4rB6Ed4GWv9tRoN0vORVo+YPgJFgKIE0E+6HJTiQ7MVfrQflSfnaik/7jTzlIcAIqLUC63sZP+ZA/KniaT7rexm8BtddvBzotmF1PBZyeKCQw7WcbgvzdbV+tN+lL9vEztAVyumPJH9mpYCQoAVagrx8jkr5kL9A/2XNaC6i9nhrkbjs1vI2HALX2Al2gD8ykd7DXAU0dZROmDym0XzoRaSKt3n86f6uHvX77kTN9l8MKXqDdc1yr73SDFWhQtEAX6P8jYo841LDjZ2jasEAX6EcDTQDbM2h6BqNbJMVjG876I73oiJHGR/tbO8VL9XjchJ4WoEBn7ydb/Wz9qIHJ3+qGjI8clIAVwBaEJgT5owlBdsqP9qf1d0+46XreHX+BHv6Fe9sABfp4R7ID4EO/9LEddfTqCWr3p1se+ZteT/vZBiG9aWLaBrP+yH+BBiIIiBSAdH2B7oQmBg72An0tF03EpzUsFX/5GZoEsYLS9faWRvHRLTVdjwWSf3M5Hc+0P1sf0mf8DG0TtgGmZ1YCkuJJJzw1IPknACh+slN8tD/Vx/qneMkeT+gC7Z4bW0Doeiow2S1w1IBkp3hSe4Fe/KGSgLEATA8Qio8aqhNatiAJJt19rS6gPeIUaFvB6+uXT2gCkuw2XfL3dKCpIWhC2/xoApP+pDetp3zt+gL9sCMHFbhAd0IfFLATjI4EKWB2vb1eTzj5wzud0PLdCVsQErhAw4Qr0LNfXRLAdmKuBpj8p/HSetKL7OR/t53i//gMkL6cRAnbgOh62o/sd/uneFI75UP2dP/V6yn+Aj38IbAT+voOnQJfoOW7D/TYKj2TpwWl9bbg0/lSfKnd5jf+2I4mFglKCVj/tB/5o4JQvOn+dj3FS09JbD7pY0Zab+Mp0PKXk6gBqAAEXLqe/Bfo8MxpC0TX3w0UAULx2glL/igea6f9rJ2OaOQvru/upxx2YsQJyzM2AUIFKtDXbyM+7shhCzp9PTWEbQACmOwEsM2f4p+Oh/a7Oz+rV3yGthtOX1+g3Rdbq/Wy9bUNRP4L9Ekhmnhkv3uCTcdjAaMGIQDtfuSvQBfogwIWsNcBnSZEHUcfGuyEunuC0n7pUwHSh/Ql/XbbKf4PfaefctgA7PUEwNMnjAUk1YfW23is/ql/ir9AwxHDCpg2EAHytHgo3mm7zT8+Q/fI4f5XJzuxbEFXN9g0sKSHzX8caCsoBUwJU0OtjofiJ/vq+AjA6TM4+aN6kl5kL9Ck0PARhQouw/m4nICZHgDWH8UX5z/9ofC3TxwSlApI6wu0+yLI6tkJLRUr0EfBSA868owPwNUTmhKmiUW3qLv9pwWgeFMApvWS/T7+Qz52/+UTmgpYoI8KFGj31Gj5c2gqCHUcrSf7av+d0NcKW33ojkL1LNCnv1AhQcmuBYffvaCGJWAoXnvHtPlRfB8AyvfTKZ7XHzkIEBKICjQNkN3Pxp/Ga+Mj/ckf5ffnJjQJSoKR4Ckg9BmCJpqNP413Wg/yR/kV6PCnrgiwuxuICk7xkD1tOPJfoOEMTAWwZ0gSPJ14FC81UIE+KvDrz9C24AQoAUINQROJ/BPg1ECkh40vvZ7ioXytXgVaKlagZ/9PmQItfxhmXLDwsZvsn49v3jqhrxXshJaEdUL/8Qktefm4PJ1INKHJv7XTflYPuz+deaftlA8NgHG9Vr+cRAmTnQqafugg/9Y+XiD5Px5MA0v52/rR9fGH9gI9+/vGBfr0GG34uT81xPIzNAVAdjsh6Ba3eoIV6JcBTYCmdguk3Y8agvzRLZP82/V0PcU7HY9taDuwMJ/pIwdtmNoL9OyfMBXoE5EkSAowdfx4h8szH8WXfmil9Z3QpyNOJ3R25ivQs8+l4wZNgZ6ewPVXBRIF4qccyeZdWwWmFSjQ04rW31YFCvRW+bv5tAIFelrR+tuqQIHeKn83n1agQE8rWn9bFSjQW+Xv5tMKFOhpRetvqwIFeqv83XxagQI9rWj9bVWgQG+Vv5tPK1CgpxWtv60KFOit8nfzaQUK9LSi9bdVgQK9Vf5uPq3AP3PXlwIEkq/DAAAAAElFTkSuQmCC"
    }
  }))
  .add("Manage Backup Code", () => {
    const backupCodeToPdf = new BackupCodePdfService();
    const code = "CBVDCH4G23MJ46FXJBR8GDX4WKBF97VR";
    return {
      component: ManageBackupCodeComponent,
      props: {
        form: fromChangePassword.initialState.form,
        hasStarted: boolean("hasStarted", false),
        hasFinished: boolean("hasFinished", false),
        code,
        downloadPDF: () => backupCodeToPdf.download(code)
      }
    };
  })
  .add("Delete Account", () => ({
    component: DeleteComponent,
    props: {
      form: fromDelete.initialState.form,
      hasStarted: boolean("hasStarted", false)
    }
  }));
storiesOf("Account/Shared", module)
  .addDecorator(withKnobs)
  .addDecorator(
    moduleMetadata({
      imports: [
        InlineSVGModule.forRoot(),
        HttpClientModule,
        NgrxFormsModule,
        SharedModule,
        RouterTestingModule,
        ProgressIndicatorModule,
        BrowserAnimationsModule,
        StoreModule.forRoot({})
      ],
      declarations: [PasswordInputComponent, DownloadBackupCodeComponent]
    })
  )
  .add("Backup Code", () => ({
    component: BackupCodeComponent,
    props: {
      code: "CBVDCH4G23MJ46FXJBR8GDX4WKBF97VR"
    }
  }))
  .add("Password Input", () => ({
    component: PasswordInputComponent,
    props: {
      showConfirm: fromChangePassword.initialState.form.controls.showConfirm,
      newPassword: fromChangePassword.initialState.form.controls.newPassword,
      newPasswordConfirm:
        fromChangePassword.initialState.form.controls.newPasswordConfirm,
      errors: fromChangePassword.initialState.form.errors
    }
  }))
  .add("Download Backup Code", () => {
    const backupCodeToPdf = new BackupCodePdfService();
    return {
      component: DownloadBackupCodeComponent,
      props: {
        hasFinished: boolean("hasFinished", true),
        downloadPDF: () =>
          backupCodeToPdf.download("CBVDCH4G23MJ46FXJBR8GDX4WKBF97VR")
      }
    };
  })
  .add("Form Label", () => {
    const defaultFormLabelState = "active";
    const formLabelControls = {
      Default: defaultFormLabelState,
      Inactive: "inactive",
      Complete: "complete"
    };
    // any type is, I think, because storybook is 4.0 but types aren't
    const formLabelControl: any = select(
      "Label State",
      formLabelControls,
      defaultFormLabelState
    );

    const defaultLabelTextState = "Password";
    const labelTextControls = {
      Short: defaultLabelTextState,
      Medium: "Verify Email Address",
      Long: "Download PDF with New Backup Code",
      "Extra Long":
        "Download PDF with New Backup Code So That You Can Forget Your Password As Long As You Don't Forget Where You Kept This",
      Weird: "🍫 ⋆ 🍭  🎀  𝒫𝒶𝓈𝓈𝒾𝓉 𝓁𝑒𝓉'𝓈 𝑔𝑒𝓉 𝓈𝒶𝒻𝑒 𝓉💙𝒹𝒶𝓎  🎀  🍭 ⋆ 🍫"
    };
    // any type is, I think, because storybook is 4.0 but types aren't
    const labelTextControl: any = select(
      "Label Text",
      labelTextControls,
      defaultLabelTextState
    );

    return {
      template: `
        <h3>Basic label</h3>

        <app-form-label
          for="nothing"
          [isLarge]="${boolean("Is Large", true)}"
          [isInactive]="${formLabelControl === "inactive" ? true : false}"
          [isComplete]="${formLabelControl === "complete" ? true : false}"
        >
          ${labelTextControl}
        </app-form-label>

        <h3>Testing out consecutive fields (these are wrapped in large form-field divs)</h3>

        <div class="form-field form-field--large">
          <app-form-label
            for="nothing"
            [isLarge]="true"
            [isInactive]="${formLabelControl === "inactive" ? true : false}"
            [isComplete]="${formLabelControl === "complete" ? true : false}"
          >
            ${labelTextControl}
          </app-form-label>
        </div>

        <div class="form-field form-field--large">
          <app-form-label
            for="nothing"
            [isLarge]="true"
            [isInactive]="${formLabelControl === "inactive" ? true : false}"
            [isComplete]="${formLabelControl === "complete" ? true : false}"
          >
            ${labelTextControl}
          </app-form-label>
        </div>

        <div class="form-field form-field--large">
          <app-form-label
            for="nothing"
            [isLarge]="true"
            [isInactive]="${formLabelControl === "inactive" ? true : false}"
            [isComplete]="${formLabelControl === "complete" ? true : false}"
          >
            ${labelTextControl}
          </app-form-label>
        </div>

        <h3>Testing out a field with input and link to the right</h3>

        <div style="max-width: 500px; margin: 0 auto;">
          <div class="form-field form-field--large">
            <div class="form-field__wrapper-for-link">
              <app-form-label for="foo" [isLarge]="true">
                ${labelTextControl}
              </app-form-label>
              <app-text-link class="form-field__label-link" tabindex="8">
                <span class="u-visible--all-but-small">Forgot? </span>Recover&nbsp;your&nbsp;account
              </app-text-link>
            </div>
            <input
              id="foo"
              type="password"
              class="form-field__input"
              tabindex="3"
            >
          </div>
        </div>
      `
    };
  });
