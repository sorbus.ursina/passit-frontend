import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-svg-demo',
  template: `
    <div>
      <figure>
        <svg class="svg svg--black">
          <use [attr.xlink:href]="iconId"></use>
        </svg>
        <figcaption>
          {{iconId}}
        </figcaption>
      </figure>
    </div>
  `,
  styleUrls: ['./svg-demo.component.scss']
})
export class SvgDemoComponent {
  @Input() iconId: string;
}
