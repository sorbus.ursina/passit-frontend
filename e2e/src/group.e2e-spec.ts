import { ExpectedConditions as EC } from "protractor";
import { browser, ElementFinder } from "protractor";

import { AppPage } from "./app.po";
import { GroupsPage } from "./group.po";
import { SecretsPage } from "./secrets.po";
import { DEFAULT_WAIT, login, register } from "./shared";

describe("List, Add, Edit and Remove Groups", () => {
  let groupsPage: GroupsPage;
  let app: AppPage;
  let secretsPage: SecretsPage;
  let USERNAME: string;
  let PASSWORD: string;

  const mockGroupFormData = [{ name: "First Test" }, { name: "Second Test" }];

  const mockEditGroupFormData = [
    { name: "First Test Edited" },
    { name: "Second Test Edited" }
  ];

  beforeAll(async () => {
    browser.sleep(50);

    secretsPage = new SecretsPage();
    groupsPage = new GroupsPage();

    USERNAME =
      Math.random()
        .toString(36)
        .slice(2) + "@example.com";
    PASSWORD = "hunterhunter22";

    await register(browser, USERNAME, PASSWORD, PASSWORD);
    await browser.refresh();
    await login(browser, USERNAME, PASSWORD);
    await groupsPage.navigateTo();
    await browser.wait(
      EC.presenceOf(groupsPage.getNewGroupBtnElem()),
      DEFAULT_WAIT,
      "new group button is displyed"
    );
  });

  beforeEach(() => {
    groupsPage = new GroupsPage();
    app = new AppPage();
  });

  afterAll(async () => {
    browser.executeScript("window.localStorage.clear();");
    browser.executeScript("window.sessionStorage.clear();");
  });

  it("in groups page and logged in", async () => {
    const currentUrl = await browser.getCurrentUrl();
    expect(currentUrl.endsWith("groups")).toBeTruthy();
  });

  it("Add groups", async () => {
    const formEle = groupsPage.getFormElem();
    let newGroupItem: ElementFinder;
    let groupTitleElem: ElementFinder;

    const displayFrom = async () => {
      await groupsPage.newGroup(browser);
      await browser.wait(
        EC.presenceOf(formEle),
        DEFAULT_WAIT,
        "form is not displyed"
      );
    };

    const createGroup = async (name: string) => {
      await groupsPage.enterGroupName(name);
      await groupsPage.submitGroupByEnter();
      await browser.wait(
        EC.stalenessOf(formEle),
        DEFAULT_WAIT,
        "group not saved and form is still open"
      );
    };

    const getGroupTitleText = () => {
      newGroupItem = groupsPage.getNewlyCreatedGroupElem();
      groupTitleElem = groupsPage.getGroupTitleElem();
      browser.sleep(50);
      const locator = groupTitleElem.locator();
      browser.sleep(50);
      return newGroupItem.element(locator).getText();
    };

    for (const data of mockGroupFormData) {
      await displayFrom();
      await createGroup(data.name);
      expect(await getGroupTitleText()).toBe(data.name);
    }
  });

  it("check if the newly added groups are added correctly", async () => {
    const count = await groupsPage.getAllGroupsElem().count();
    for (let index = 0; index < count; index++) {
      const groupElem = groupsPage.getAllGroupsElem().get(index!);
      const currentGroupTitleElem = groupElem!.element(
        groupsPage.getGroupTitleElem().locator()
      );
      expect(await currentGroupTitleElem.getText()).toBe(
        mockGroupFormData[index].name
      );
    }
  });

  it("open, close and check group edit form: to confirm form has correct details and display toggle works", async () => {
    const count = await groupsPage.getAllGroupsElem().count();
    for (let index = 0; index < count; index++) {
      const groupElem = groupsPage.getAllGroupsElem().get(index!);
      const toggleBtn = groupsPage.getManageGroupToggleBtnElem().locator();
      const currentGroupToggleBtnElem = groupElem!.element(toggleBtn);
      // Check no form item is displayed initially
      expect(await groupsPage.getFormElem().isPresent()).toBeFalsy();
      expect(await currentGroupToggleBtnElem.getText()).toBe("Manage");

      // Open form and check form details
      await currentGroupToggleBtnElem.click();
      await browser.wait(
        EC.presenceOf(groupsPage.getFormElem()),
        DEFAULT_WAIT,
        "form is not opened"
      );

      const value = await groupsPage
        .getNameFormInputElem()
        .getAttribute("value");
      expect(value).toBe(mockGroupFormData[index!].name);
      expect(await currentGroupToggleBtnElem.getText()).toBe("Close");

      // close form and confirm if its done properly
      await currentGroupToggleBtnElem.click();
      await browser.wait(
        EC.stalenessOf(groupsPage.getFormElem()),
        DEFAULT_WAIT,
        "form is not closed"
      );
      expect(await currentGroupToggleBtnElem.getText()).toBe("Manage");
    }
  });

  it("group in edit mode opened should be closed if url changed to list page and returned back to groups page", async () => {
    let firstGroupItem: ElementFinder;
    let currentGroupToggleBtnElem: ElementFinder;

    firstGroupItem = groupsPage.getAllGroupsElem().first();
    currentGroupToggleBtnElem = firstGroupItem.element(
      groupsPage.getManageGroupToggleBtnElem().locator()
    );
    await currentGroupToggleBtnElem.click();
    await browser.wait(
      EC.presenceOf(groupsPage.getFormElem()),
      DEFAULT_WAIT,
      "form is not opened"
    );

    await app.clickPasswordsNavLinkElem();
    await EC.browser.wait(
      EC.presenceOf(secretsPage.getSearchElem()),
      DEFAULT_WAIT,
      "Search secrets should be visible"
    );
    await groupsPage.navigateTo();
    expect(
      await groupsPage.getAllGroupsElem().getAttribute("class")
    ).not.toContain("secret-form--is-active");
  });

  it("edit all groups", async () => {
    const count = await groupsPage.getAllGroupsElem().count();
    for (let index = 0; index < count; index++) {
      let currentGroupToggleBtnElem: ElementFinder;
      let currentElement: ElementFinder;
      let currentSaveBtnElem: ElementFinder;
      let nextGroupItem: ElementFinder;

      currentElement = groupsPage.getAllGroupsElem().get(index!);
      await browser.sleep(100);
      currentGroupToggleBtnElem = currentElement.element(
        groupsPage.getManageGroupToggleBtnElem().locator()
      );
      await browser.wait(EC.elementToBeClickable(currentGroupToggleBtnElem));
      currentSaveBtnElem = currentElement.element(
        groupsPage.saveButton().locator()
      );
      await currentGroupToggleBtnElem.click();
      await browser.wait(
        EC.presenceOf(groupsPage.getFormElem()),
        DEFAULT_WAIT,
        "form is not opened"
      );
      expect(
        await groupsPage.getNameFormInputElem().getAttribute("value")
      ).toBe(mockGroupFormData[index!].name);

      await groupsPage.getNameFormInputElem().clear();
      await groupsPage.editGroupName(mockEditGroupFormData[index!].name);
      expect(
        await groupsPage.getNameFormInputElem().getAttribute("value")
      ).toBe(mockEditGroupFormData[index!].name);
      await browser.wait(EC.elementToBeClickable(currentSaveBtnElem));
      await currentSaveBtnElem.click();
      await browser.sleep(100);

      if (index! + 1 < count - 1) {
        nextGroupItem = groupsPage.getAllGroupsElem().get(index! + 1);
        currentGroupToggleBtnElem = nextGroupItem.element(
          groupsPage.getManageGroupToggleBtnElem().locator()
        );
        await currentGroupToggleBtnElem.click();
        await browser.wait(
          EC.presenceOf(groupsPage.getFormElem()),
          DEFAULT_WAIT,
          "form is still open"
        );
      }
    }
  });

  it("check if edited groups are added correctly", async () => {
    const count = await groupsPage.getAllGroupsElem().count();
    for (let index = 0; index < count; index++) {
      const groupElem = groupsPage.getAllGroupsElem().get(index!);
      const currentGroupTitleElem = groupElem!.element(
        groupsPage.getGroupTitleElem().locator()
      );
      expect(await currentGroupTitleElem.getText()).toBe(
        mockEditGroupFormData[index!].name
      );
    }
  });

  it("remove groups: first dismiss alerts then click delete button again and accept alert", async () => {
    const count = await groupsPage.getAllGroupsElem().count();
    for (let groupIndex = 0; groupIndex < count; groupIndex++) {
      await browser.sleep(100);
      const currentGroupItem = groupsPage.getAllGroupsElem().first();
      const currentGroupToggleBtnElem = currentGroupItem.element(
        groupsPage.getManageGroupToggleBtnElem().locator()
      );
      const currentGroupTitleElem = currentGroupItem.element(
        groupsPage.getGroupTitleElem().locator()
      );

      expect(await currentGroupTitleElem.getText()).toBe(
        mockEditGroupFormData[groupIndex].name
      );
      await currentGroupToggleBtnElem.click();
      await browser.wait(
        EC.presenceOf(groupsPage.getFormElem()),
        DEFAULT_WAIT,
        "form is not opened"
      );

      await groupsPage.deleteGroupByClick();
      await browser.wait(
        EC.alertIsPresent(),
        DEFAULT_WAIT,
        "Alert is not displayed for group delete"
      );
      await browser
        .switchTo()
        .alert()
        .dismiss();

      await groupsPage.deleteGroupByClick();
      await browser.wait(
        EC.alertIsPresent(),
        DEFAULT_WAIT,
        "Alert is not displayed for group delete"
      );
      await browser
        .switchTo()
        .alert()
        .accept();
      await browser.sleep(10000);
    }
  });
});
